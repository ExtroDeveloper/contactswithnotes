package com.notesforcontacts.sample;

import org.apache.commons.lang3.builder.EqualsBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Utils
{
    public static void assertEquals(Object object, Object toCompare)
    {
        assertThat(object.equals(toCompare), is(EqualsBuilder.reflectionEquals(object, toCompare)));
        assertThat(toCompare.equals(object), is(EqualsBuilder.reflectionEquals(toCompare, object)));
    }
}
