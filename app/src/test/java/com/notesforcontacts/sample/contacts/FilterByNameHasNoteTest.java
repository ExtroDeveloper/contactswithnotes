package com.notesforcontacts.sample.contacts;

import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class FilterByNameHasNoteTest
{
    @Test
    public void nameContains() throws Exception
    {
        FilterByNameHasNote filterByNameHasNote = new FilterByNameHasNote("111", false);

        String id = "";
        assertThat(filterByNameHasNote.suitable(new ContactInfo(id, Collections.emptyList(), "111a", 1, null)), is(true));
        assertThat(filterByNameHasNote.suitable(new ContactInfo(id, Collections.emptyList(), "a111a", 1, null)), is(true));
        assertThat(filterByNameHasNote.suitable(new ContactInfo(id, Collections.emptyList(), "a111", 1, null)), is(true));
        assertThat(filterByNameHasNote.suitable(new ContactInfo(id, Collections.emptyList(), "a121", 1, null)), is(false));
    }

}