package com.notesforcontacts.sample.ui.notelist;

import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModel;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModelFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(RobolectricTestRunner.class)
public class ViewModelTest
{
    @Test
    public void testUserName() throws Exception
    {
        ViewModel viewModel = new ViewModel(mock(NoteViewModelFactory.class), mock(NoteAdder.class));
        String name = "Name";
        viewModel.setUserName(name);

        assertThat(viewModel.getUserName(), is(name));
    }

    @Test
    @Config(sdk = 23)
    public void testFactoryIsUsedToCreateNoteViewModel() throws Exception
    {
        NoteViewModelFactory noteViewModelFactoryMock = mock(NoteViewModelFactory.class);

        ViewModel viewModel = new ViewModel(noteViewModelFactoryMock, mock(NoteAdder.class));

        List<NoteForContact> list = new ArrayList<>();
        NoteForContact noteForContact1 = new NoteForContact(111, "qwe", true);
        list.add(noteForContact1);
        NoteForContact noteForContact2 = new NoteForContact(222, "asd", true);
        list.add(noteForContact2);

        NoteViewModel noteViewModelMock1 = mock(NoteViewModel.class);
        NoteViewModel noteViewModelMock2 = mock(NoteViewModel.class);

        when(noteViewModelFactoryMock.createViewModel(noteForContact1)).thenAnswer(invocation -> noteViewModelMock1);
        when(noteViewModelFactoryMock.createViewModel(noteForContact2)).thenAnswer(invocation -> noteViewModelMock2);

        viewModel.setNotes(list);

        List<NoteViewModel> noteViewModels = viewModel.getNoteViewModels();
        for (int i = 0; i < list.size(); ++i)
        {
            assertThat(noteViewModels.get(0), is(noteViewModelMock1));
            assertThat(noteViewModels.get(1), is(noteViewModelMock2));
        }

        assertThat(noteViewModels.size(), is(list.size()));
    }

    @Test
    @Config(sdk = 23)
    public void testAddNoteClickFiresNoteAdder() throws Exception
    {
        NoteAdder noteAdderMock = mock(NoteAdder.class);

        ViewModel viewModel = new ViewModel(mock(NoteViewModelFactory.class), noteAdderMock);
        viewModel.onAddNoteClick();

        verify(noteAdderMock, times(1)).addNewNote();
    }
}