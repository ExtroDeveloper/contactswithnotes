package com.notesforcontacts.sample.ui.contactpage;

import android.support.v4.app.Fragment;

import com.notesforcontacts.sample.contacts.ContactInfo;
import com.notesforcontacts.sample.ui.contactpage.contacts.ContactViewModel;
import com.notesforcontacts.sample.ui.contactpage.contacts.ContactViewModelCallback;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ContactViewModelFactory.class})
public class ContactViewModelFactoryTest
{
    @Test
    public void viewCreatedModelFields() throws Exception
    {
        NoteListPageOpenRequester noteListPageOpenRequester = mock(NoteListPageOpenRequester.class);
        ContactActionShower contactActionShower = new ContactActionShower(mock(Fragment.class));
        ContactViewModelFactory contactViewModelFactory = new ContactViewModelFactory(noteListPageOpenRequester, contactActionShower);

        ContactViewModel viewModelMock = PowerMockito.mock(ContactViewModel.class);
        whenNew(ContactViewModel.class).withAnyArguments().thenReturn(viewModelMock);

        int noteCount = 2;
        ContactInfo contactInfo = new ContactInfo("1", Collections.emptyList(), "mame", noteCount, null);

        ContactViewModel.Callback callbackMock = new ContactViewModelCallback(contactInfo.id, noteListPageOpenRequester, contactActionShower);

        contactViewModelFactory.createViewModel(contactInfo);

        verifyNew(ContactViewModel.class).withArguments(contactInfo.name, contactInfo.phoneNumberToString(), String.valueOf(noteCount), null, callbackMock);
    }
}