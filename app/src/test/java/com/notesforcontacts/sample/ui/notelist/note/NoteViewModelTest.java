package com.notesforcontacts.sample.ui.notelist.note;

import android.databinding.Observable;
import android.support.annotation.NonNull;

import com.notesforcontacts.sample.BR;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class NoteViewModelTest
{
    @Test
    public void onTextClick() throws Exception
    {
        NoteViewModel.Callback callbackMock = mock(NoteViewModel.Callback.class);
        NoteViewModel viewModel = new NoteViewModel(11, callbackMock);

        viewModel.onTextClick();

        verify(callbackMock, times(1)).onTextPressed();
    }

    @Test
    public void onDeleteClick() throws Exception
    {
        NoteViewModel.Callback callbackMock = mock(NoteViewModel.Callback.class);
        NoteViewModel viewModel = new NoteViewModel(11, callbackMock);

        viewModel.onDeleteClick();

        verify(callbackMock, times(1)).onDeleteNoteButton();
    }

    @Test
    public void onEditClick() throws Exception
    {
        NoteViewModel.Callback callbackMock = mock(NoteViewModel.Callback.class);
        NoteViewModel viewModel = new NoteViewModel(11, callbackMock);
        String text = "text";
        viewModel.setText(text);

        viewModel.onEditClick();

        verify(callbackMock, times(1)).onEditNoteButton(text);
    }

    @Test
    public void onLongClick() throws Exception
    {
        NoteViewModel.Callback callbackMock = mock(NoteViewModel.Callback.class);
        NoteViewModel viewModel = new NoteViewModel(11, callbackMock);
        String text = "text";
        viewModel.setText(text);

        viewModel.onLongClick();

        verify(callbackMock, times(1)).onLongClick(text);
    }

    @Test
    public void textOnPropertyChangedFiresForTextFieldOnSettingChecked() throws Exception
    {
        NoteViewModel noteViewModel = getNoteViewModel();

        final boolean[] textChangedNotifyed = {false};

        noteViewModel.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback()
        {
            @Override
            public void onPropertyChanged(Observable observable, int i)
            {
                if (i == BR.text)
                {
                    textChangedNotifyed[0] = true;
                }
            }
        });

        noteViewModel.setChecked(true);

        assertThat(textChangedNotifyed[0], is(true));
    }

    @NonNull
    private NoteViewModel getNoteViewModel()
    {
        return new NoteViewModel(11, mock(NoteViewModel.Callback.class));
    }
}