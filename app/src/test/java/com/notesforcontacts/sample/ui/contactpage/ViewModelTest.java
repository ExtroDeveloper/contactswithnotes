package com.notesforcontacts.sample.ui.contactpage;

import com.notesforcontacts.sample.contacts.ContactInfo;
import com.notesforcontacts.sample.ui.contactpage.contacts.ContactViewModel;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ViewModelTest
{
    @Test
    public void setContactInfos() throws Exception
    {
        ContactViewModelFactory contactViewModelFactoryMock = mock(ContactViewModelFactory.class);
        ViewModel viewModel = new ViewModel(contactViewModelFactoryMock);

        List<ContactInfo> list = new ArrayList<>();
        ContactInfo noteForContact1 = new ContactInfo("111", Collections.emptyList(), "name1", 0, null);
        list.add(noteForContact1);
        ContactInfo noteForContact2 = new ContactInfo("222", Collections.emptyList(), "name2", 3, null);
        list.add(noteForContact2);

        ContactViewModel contactViewModelMock1 = mock(ContactViewModel.class);
        ContactViewModel contactViewModelMock2 = mock(ContactViewModel.class);

        when(contactViewModelFactoryMock.createViewModel(noteForContact1)).thenReturn(contactViewModelMock1);
        when(contactViewModelFactoryMock.createViewModel(noteForContact2)).thenReturn(contactViewModelMock2);

        viewModel.setContactInfos(list);

        List<ContactViewModel> noteViewModels = viewModel.getContactViewModels();

        for (int i = 0; i < list.size(); ++i)
        {
            assertThat(noteViewModels.get(0), is(contactViewModelMock1));
            assertThat(noteViewModels.get(1), is(contactViewModelMock2));
        }

        assertThat(noteViewModels.size(), is(list.size()));
    }
}