package com.notesforcontacts.sample.ui.contactpage;

import org.junit.Test;
import static com.notesforcontacts.sample.Utils.assertEquals;

public class ContactClickEventTest
{
    @Test
    public void equals() throws Exception
    {
        String contactId = "111";
        ContactClickEvent contactClickEvent = new ContactClickEvent(contactId);

        assertEquals(contactClickEvent, contactClickEvent);
    }
}