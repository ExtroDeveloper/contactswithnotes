package com.notesforcontacts.sample.ui.contactpage;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.Storages.FilterStorage;
import com.notesforcontacts.sample.contacts.FilterByNameHasNote;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import rx.Observable;
import rx.subjects.BehaviorSubject;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Observable.class})
public class FilterIconHighLighterTest
{
    @Test
    public void checkHighlightMethodIsCalledWhenMenuReady() throws Exception
    {
        Observable<Void> voidObservable = Observable.just(null);

        ViewModel viewModelMock = mock(ViewModel.class);

        BehaviorSubject<FilterByNameHasNote> filterObservableMock = BehaviorSubject.create();

        FilterIconHighLighter filterIconHighLighter = getFilterIconHighLighter(voidObservable, viewModelMock, filterObservableMock);

        filterIconHighLighter.resume();

        filterObservableMock.onNext(new FilterByNameHasNote("a", false));
        verify(viewModelMock, times(1)).setHighlightFilterIcon(true);
        filterObservableMock.onNext(new FilterByNameHasNote("s", false));
        verify(viewModelMock, times(2)).setHighlightFilterIcon(true);

        filterIconHighLighter.stop();
        filterIconHighLighter.resume();

        verify(viewModelMock, times(3)).setHighlightFilterIcon(true);

        filterObservableMock.onNext(new FilterByNameHasNote("", false));
        verify(viewModelMock, times(1)).setHighlightFilterIcon(false);

        filterIconHighLighter.stop();
        filterIconHighLighter.resume();

        verify(viewModelMock, times(2)).setHighlightFilterIcon(false);
    }

    @Test
    public void checkHighlightMethodIsCalledWhenMenuNotReady() throws Exception
    {
        Observable<Void> voidObservable = Observable.never();

        ViewModel viewModelMock = mock(ViewModel.class);

        BehaviorSubject<FilterByNameHasNote> filterObservableMock = BehaviorSubject.create();

        FilterIconHighLighter filterIconHighLighter = getFilterIconHighLighter(voidObservable, viewModelMock, filterObservableMock);

        filterIconHighLighter.resume();

        filterObservableMock.onNext(new FilterByNameHasNote("a", false));
        verify(viewModelMock, times(0)).setHighlightFilterIcon(true);

        filterObservableMock.onNext(new FilterByNameHasNote("", false));
        verify(viewModelMock, times(0)).setHighlightFilterIcon(false);

        filterObservableMock.onNext(new FilterByNameHasNote("", true));
        verify(viewModelMock, times(0)).setHighlightFilterIcon(true);
    }

    @NonNull
    private FilterIconHighLighter getFilterIconHighLighter(Observable<Void> voidObservable, ViewModel viewModelMock, BehaviorSubject<FilterByNameHasNote> filterObservableMock)
    {
        FilterStorage filterStorageMock = mock(FilterStorage.class);

        when(filterStorageMock.getObservable()).thenReturn(filterObservableMock);

        return new FilterIconHighLighter(viewModelMock, filterStorageMock, voidObservable);
    }
}