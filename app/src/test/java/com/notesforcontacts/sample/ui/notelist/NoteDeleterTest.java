package com.notesforcontacts.sample.ui.notelist;

import com.notesforcontacts.sample.ui.notelist.model.Model;
import com.notesforcontacts.sample.ui.notelist.note.NoteDeleter;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class NoteDeleterTest
{
    @Test
    public void deleteNote() throws Exception
    {
        Model modelMock = mock(Model.class);
        NoteDeleter noteDeleter = new NoteDeleter(modelMock);
        long id = 1;
        noteDeleter.deleteNote(id);

        verify(modelMock, times(1)).deleteNote(id);

        long id2 = 2;
        noteDeleter.deleteNote(id2);

        verify(modelMock, times(1)).deleteNote(id2);
    }
}