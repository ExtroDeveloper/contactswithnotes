package com.notesforcontacts.sample.ui.notelist;

import com.notesforcontacts.sample.ui.notelist.features.ShareNoteAction;
import com.notesforcontacts.sample.ui.notelist.note.LongClickAction;
import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.ui.notelist.features.EditNoteDialogShower;
import com.notesforcontacts.sample.ui.notelist.features.FragmentEditNoteDialogShower;
import com.notesforcontacts.sample.ui.notelist.note.NoteDeleter;
import com.notesforcontacts.sample.ui.notelist.note.NoteCheckedStateInverter;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModel;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModelCallback;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModelFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({NoteViewModelFactory.class})
public class NoteViewModelFactoryTest
{
    @Test
    public void viewCreatedModelFields() throws Exception
    {
        EditNoteDialogShower editNoteDialogShower = mock(FragmentEditNoteDialogShower.class);
        NoteDeleter noteDeleter = mock(NoteDeleter.class);
        NoteCheckedStateInverter noteCheckedStateInverter = mock(NoteCheckedStateInverter.class);
        LongClickAction longClickAction = mock(ShareNoteAction.class);
        NoteViewModelFactory noteViewModelFactory = new NoteViewModelFactory(editNoteDialogShower, noteDeleter, noteCheckedStateInverter, longClickAction);

        NoteViewModel viewModelMock = PowerMockito.mock(NoteViewModel.class);
        whenNew(NoteViewModel.class).withAnyArguments().thenReturn(viewModelMock);

        NoteForContact noteForContact = new NoteForContact(1, "", true);
        noteViewModelFactory.createViewModel(noteForContact);

        NoteViewModelCallback noteViewModelCallback = new NoteViewModelCallback(noteForContact.id, noteCheckedStateInverter, editNoteDialogShower, noteDeleter, longClickAction);

        verifyNew(NoteViewModel.class).withArguments(noteForContact.id, noteViewModelCallback);
        verify(viewModelMock).setText(noteForContact.text);
    }
}