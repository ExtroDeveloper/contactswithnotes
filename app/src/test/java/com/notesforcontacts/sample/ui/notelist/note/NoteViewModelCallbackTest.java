package com.notesforcontacts.sample.ui.notelist.note;

import com.notesforcontacts.sample.ui.notelist.features.ShareNoteAction;
import com.notesforcontacts.sample.ui.notelist.features.EditNoteDialogShower;

import org.junit.Test;

import static com.notesforcontacts.sample.Utils.assertEquals;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class NoteViewModelCallbackTest
{
    @Test
    public void onTextPressed() throws Exception
    {
        long noteId = 10;
        NoteCheckedStateInverter noteCheckedStateInverterMock = mock(NoteCheckedStateInverter.class);
        EditNoteDialogShower editNoteDialogShowerMock = mock(EditNoteDialogShower.class);
        NoteDeleter noteDeleterMock = mock(NoteDeleter.class);
        LongClickAction longClickActionMock = mock(ShareNoteAction.class);

        NoteViewModelCallback noteViewModelCallback = new NoteViewModelCallback(noteId, noteCheckedStateInverterMock, editNoteDialogShowerMock, noteDeleterMock, longClickActionMock);

        noteViewModelCallback.onTextPressed();

        verify(noteCheckedStateInverterMock, times(1)).invertCheckedState(noteId);
    }

    @Test
    public void onDeleteNoteButton() throws Exception
    {
        long noteId = 10;
        NoteCheckedStateInverter noteCheckedStateInverterMock = mock(NoteCheckedStateInverter.class);
        EditNoteDialogShower editNoteDialogShowerMock = mock(EditNoteDialogShower.class);
        NoteDeleter noteDeleterMock = mock(NoteDeleter.class);
        LongClickAction longClickActionMock = mock(ShareNoteAction.class);

        NoteViewModelCallback noteViewModelCallback = new NoteViewModelCallback(noteId, noteCheckedStateInverterMock, editNoteDialogShowerMock, noteDeleterMock, longClickActionMock);

        noteViewModelCallback.onDeleteNoteButton();

        verify(noteDeleterMock, times(1)).deleteNote(noteId);
    }

    @Test
    public void onEditNoteButton() throws Exception
    {
        long noteId = 10;
        NoteCheckedStateInverter noteCheckedStateInverterMock = mock(NoteCheckedStateInverter.class);
        EditNoteDialogShower editNoteDialogShowerMock = mock(EditNoteDialogShower.class);
        NoteDeleter noteDeleterMock = mock(NoteDeleter.class);
        LongClickAction longClickActionMock = mock(ShareNoteAction.class);

        NoteViewModelCallback noteViewModelCallback = new NoteViewModelCallback(noteId, noteCheckedStateInverterMock, editNoteDialogShowerMock, noteDeleterMock, longClickActionMock);

        String text = "text";
        noteViewModelCallback.onEditNoteButton(text);

        verify(editNoteDialogShowerMock, times(1)).showNoteEditDialog(noteId, text);
    }

    @Test
    public void equals() throws Exception
    {
        long noteId = 10;
        NoteCheckedStateInverter noteCheckedStateInverterMock = mock(NoteCheckedStateInverter.class);
        EditNoteDialogShower editNoteDialogShowerMock = mock(EditNoteDialogShower.class);
        NoteDeleter noteDeleterMock = mock(NoteDeleter.class);
        LongClickAction longClickActionMock = mock(ShareNoteAction.class);

        NoteViewModelCallback noteViewModelCallback = new NoteViewModelCallback(noteId, noteCheckedStateInverterMock, editNoteDialogShowerMock, noteDeleterMock, longClickActionMock);
        assertEquals(noteViewModelCallback, noteViewModelCallback);

        NoteViewModelCallback toCompare = new NoteViewModelCallback(1, noteCheckedStateInverterMock, editNoteDialogShowerMock, noteDeleterMock, longClickActionMock);;
        assertEquals(noteViewModelCallback, toCompare);

        toCompare = new NoteViewModelCallback(noteId, mock(NoteCheckedStateInverter.class), editNoteDialogShowerMock, noteDeleterMock, longClickActionMock);
        assertEquals(noteViewModelCallback, toCompare);

        toCompare = new NoteViewModelCallback(noteId, noteCheckedStateInverterMock, mock(EditNoteDialogShower.class), noteDeleterMock, longClickActionMock);
        assertEquals(noteViewModelCallback, toCompare);

        toCompare = new NoteViewModelCallback(noteId, noteCheckedStateInverterMock, editNoteDialogShowerMock, mock(NoteDeleter.class), longClickActionMock);
        assertEquals(noteViewModelCallback, toCompare);

        toCompare = new NoteViewModelCallback(noteId, noteCheckedStateInverterMock, editNoteDialogShowerMock, noteDeleterMock, mock(ShareNoteAction.class));
        assertEquals(noteViewModelCallback, toCompare);
    }
}