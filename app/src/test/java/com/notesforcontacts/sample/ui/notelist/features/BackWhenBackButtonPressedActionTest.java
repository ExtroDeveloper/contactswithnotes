package com.notesforcontacts.sample.ui.notelist.features;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.notesforcontacts.sample.ui.main.MainActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(RobolectricTestRunner.class)
public class BackWhenBackButtonPressedActionTest
{
    @Test
    @Config(sdk = 23)
    public void testActivityBackPresserRunsOnEvent() throws Exception
    {
        AppCompatActivity mainActivity = mock(MainActivity.class);

        Fragment fragment = mock(Fragment.class);
        when(fragment.getActivity()).thenReturn(mainActivity);

        BackWhenBackButtonPressedAction backWhenBackButtonPressedAction = new BackWhenBackButtonPressedAction(fragment);
        backWhenBackButtonPressedAction.run();

        verify(mainActivity, times(1)).onBackPressed();
    }
}