package com.notesforcontacts.sample.ui.contactpage;

import com.notesforcontacts.sample.contacts.ContactInfo;
import com.notesforcontacts.sample.ui.contactpage.model.Model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Observable.class})
public class ContactListShowerTest
{
    @Test
    @Config(sdk = 23)
    public void subscribeAndUnsubscribeOnResumeAndOnPause() throws Exception
    {
        Model modelMock = mock(Model.class);

        Observable<List<ContactInfo>> observableMock = mock(Observable.class);
        when(modelMock.getObservableContactInfoList()).thenReturn(observableMock);

        Subscription subscriptionMock = mock(Subscription.class);
        when(observableMock.subscribe(any(Action1.class))).thenAnswer(invocation -> subscriptionMock);

        ContactListShower contactListShower = new ContactListShower(mock(ViewModel.class), modelMock);

        verify(observableMock, times(0)).subscribe(any(Action1.class));

        contactListShower.resume();

        verify(observableMock, times(1)).subscribe(any(Action1.class));

        contactListShower.stop();

        verify(subscriptionMock, times(1)).unsubscribe();
    }

    @Test
    @Config(sdk = 23)
    public void viewModelIsUpdated() throws Exception
    {
        ViewModel viewModelMock = mock(ViewModel.class);
        Model modelMock = mock(Model.class);

        PublishSubject<List<ContactInfo>> subject = PublishSubject.create();

        when(modelMock.getObservableContactInfoList()).thenReturn(subject);

        ContactListShower contactListShower = new ContactListShower(viewModelMock, modelMock);

        List<ContactInfo> list = new ArrayList<>();

        verify(viewModelMock, times(0)).setContactInfos(list);

        contactListShower.resume();

        subject.onNext(list);

        verify(viewModelMock, times(1)).setContactInfos(list);
    }
}