package com.notesforcontacts.sample.ui.notelist;

import com.notesforcontacts.sample.Utilits.EventManager;
import com.notesforcontacts.sample.ui.notelist.features.AddNoteRequestEvent;
import com.notesforcontacts.sample.ui.notelist.features.UpdateNoteTextRequestEvent;
import com.notesforcontacts.sample.ui.notelist.model.Model;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class NoteEventHandlerTest
{
    @Test
    public void fireModelAddNoteMethodOnAddNoteEvent() throws Exception
    {
        Model modelMock = mock(Model.class);
        NoteEventHandler noteEventHandler = new NoteEventHandler(modelMock);

        noteEventHandler.resume();

        String text = "sourceText";
        EventManager.postEvent(new AddNoteRequestEvent(text));

        verify(modelMock, times(1)).addNote(text);

        noteEventHandler.stop();

        EventManager.postEvent(new AddNoteRequestEvent(text));

        verify(modelMock, times(1)).addNote(text);
    }

    @Test
    public void resume() throws Exception
    {
        Model modelMock = mock(Model.class);
        NoteEventHandler noteEventHandler = new NoteEventHandler(modelMock);

        noteEventHandler.resume();

        long noteId = 11;
        String text = "sourceText";

        EventManager.postEvent(new UpdateNoteTextRequestEvent(noteId, text));

        verify(modelMock, times(1)).updateNoteText(noteId, text);

        noteEventHandler.stop();

        EventManager.postEvent(new UpdateNoteTextRequestEvent(noteId, text));

        verify(modelMock, times(1)).updateNoteText(noteId, text);
    }
}