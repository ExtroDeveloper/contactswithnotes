package com.notesforcontacts.sample.ui.notelist.note;

import com.notesforcontacts.sample.ui.notelist.model.Model;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class NoteCheckedStateInverterTest
{
    @Test
    public void invertCheckedState() throws Exception
    {
        Model modelMock = mock(Model.class);
        NoteCheckedStateInverter noteCheckedStateInverter = new NoteCheckedStateInverter(modelMock);

        long id = 11;
        noteCheckedStateInverter.invertCheckedState(id);

        verify(modelMock, times(1)).invertCheckedState(id);
    }
}