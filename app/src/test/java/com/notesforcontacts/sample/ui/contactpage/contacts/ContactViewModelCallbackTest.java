package com.notesforcontacts.sample.ui.contactpage.contacts;

import com.notesforcontacts.sample.ui.contactpage.ContactActionShower;
import com.notesforcontacts.sample.ui.contactpage.NoteListPageOpenRequester;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ContactViewModelCallbackTest
{
    @Test
    public void onClick() throws Exception
    {
        String contactId = "123";
        NoteListPageOpenRequester noteListPageOpenRequester = mock(NoteListPageOpenRequester.class);
        ContactActionShower contactActionShower =  mock(ContactActionShower.class);

        ContactViewModelCallback contactViewModelCallback = new ContactViewModelCallback(contactId, noteListPageOpenRequester, contactActionShower);

        contactViewModelCallback.onClick();

        verify(noteListPageOpenRequester, times(1)).requestToOpenNoteListForContact(contactId);

        contactViewModelCallback.onLongClick();

        verify(contactActionShower, times(1)).showAction(contactId);
    }

    @Test
    public void equals() throws Exception
    {
        String contactId = "123";
        NoteListPageOpenRequester noteListPageOpenRequesterMock = mock(NoteListPageOpenRequester.class);
        ContactActionShower contactActionShowerMock =  mock(ContactActionShower.class);
        ContactViewModelCallback contactViewModelCallback = new ContactViewModelCallback(contactId, noteListPageOpenRequesterMock, contactActionShowerMock);

        ContactViewModelCallback toCompare = contactViewModelCallback;
        assertThat(contactViewModelCallback.equals(toCompare), is(EqualsBuilder.reflectionEquals(contactViewModelCallback, toCompare)));

        toCompare =  new ContactViewModelCallback(null, noteListPageOpenRequesterMock, contactActionShowerMock);
        assertThat(contactViewModelCallback.equals(toCompare), is(EqualsBuilder.reflectionEquals(contactViewModelCallback, toCompare)));
        assertThat(contactViewModelCallback.equals(toCompare), is(EqualsBuilder.reflectionEquals(toCompare, contactViewModelCallback)));

        toCompare =  new ContactViewModelCallback(contactId, null, contactActionShowerMock);
        assertThat(contactViewModelCallback.equals(toCompare), is(EqualsBuilder.reflectionEquals(contactViewModelCallback, toCompare)));
        assertThat(contactViewModelCallback.equals(toCompare), is(EqualsBuilder.reflectionEquals(toCompare, contactViewModelCallback)));

        toCompare =  new ContactViewModelCallback(contactId, noteListPageOpenRequesterMock, null);
        assertThat(contactViewModelCallback.equals(toCompare), is(EqualsBuilder.reflectionEquals(contactViewModelCallback, toCompare)));
        assertThat(contactViewModelCallback.equals(toCompare), is(EqualsBuilder.reflectionEquals(toCompare, contactViewModelCallback)));
    }
}