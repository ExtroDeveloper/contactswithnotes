package com.notesforcontacts.sample.ui.contactpage;

import com.notesforcontacts.sample.Utilits.EventManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

@PrepareForTest({EventManager.class})
@RunWith(PowerMockRunner.class)
public class NoteListPageOpenRequesterTest
{
    @Test
    public void requestToOpenNoteListForContact() throws Exception
    {
        PowerMockito.mockStatic(EventManager.class);
        NoteListPageOpenRequester noteListPageOpenRequester = new NoteListPageOpenRequester();

        String contactId = "11";
        noteListPageOpenRequester.requestToOpenNoteListForContact(contactId);

        PowerMockito.verifyStatic(times(1));

        EventManager.postEvent(new ContactClickEvent(contactId));
    }
}