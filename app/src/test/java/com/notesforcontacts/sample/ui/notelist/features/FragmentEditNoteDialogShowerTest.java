package com.notesforcontacts.sample.ui.notelist.features;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.os.OperationCanceledException;

import com.notesforcontacts.sample.ui.common.editnotedialog.Params;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static org.powermock.api.support.membermodification.MemberMatcher.method;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EditNoteDialogBuilder.class, Bundle.class, EditNoteDialog.class, FragmentEditNoteDialogShower.class})
public class FragmentEditNoteDialogShowerTest
{
    @Test
    public void showNoteEditDialog() throws Exception
    {
        EditNoteDialogShower editNoteDialogShower = new FragmentEditNoteDialogShower(mock(Fragment.class));
        whenNew(EditNoteDialogBuilder.class).withAnyArguments().thenThrow(new OperationCanceledException());

        long noteId = 1;
        String text = "sourceText";
        try
        {
            editNoteDialogShower.showNoteEditDialog(noteId, text);
        }
        catch (OperationCanceledException e)
        {
        }

        Params params = new Params(noteId, text);

        verifyNew(EditNoteDialogBuilder.class).withArguments(params);
    }

    @Test
    public void showAddNoteEditDialog() throws Exception
    {
        EditNoteDialogShower editNoteDialogShower = new FragmentEditNoteDialogShower(mock(Fragment.class));
        whenNew(EditNoteDialogBuilder.class).withAnyArguments().thenThrow(new OperationCanceledException());

        String text = "";
        try
        {
            editNoteDialogShower.showAddNoteEditDialog();
        }
        catch (OperationCanceledException e)
        {
        }

        Params params = new Params(Params.noNoteId, text);

        verifyNew(EditNoteDialogBuilder.class).withArguments(params);
    }

    @Test
    public void showEditDialogIsShown() throws Exception
    {
        EditNoteDialogShower editNoteDialogShower = new FragmentEditNoteDialogShower(mock(Fragment.class));

        EditNoteDialog mockNoteDialog = mock(EditNoteDialog.class);
        stub(method(EditNoteDialogBuilder.class, "build")).toReturn(mockNoteDialog);

        editNoteDialogShower.showNoteEditDialog(1, "");

        verify(mockNoteDialog, times(1)).show(any(FragmentManager.class), anyObject());
    }
}