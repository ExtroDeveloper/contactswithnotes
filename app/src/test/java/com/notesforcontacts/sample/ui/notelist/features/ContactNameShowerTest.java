package com.notesforcontacts.sample.ui.notelist.features;

import com.notesforcontacts.sample.ui.notelist.NoteAdder;
import com.notesforcontacts.sample.ui.notelist.ViewModel;
import com.notesforcontacts.sample.ui.notelist.model.Model;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModelFactory;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ContactNameShowerTest
{
    @Test
    public void testViewModelShowsName() throws Exception
    {
        Model model = mock(Model.class);
        NoteViewModelFactory noteViewModelFactory = mock(NoteViewModelFactory.class);
        ViewModel viewModel = new ViewModel(noteViewModelFactory, mock(NoteAdder.class));

        String name = "111";
        when(model.getContactName()).thenAnswer(invocation -> name);

        ContactNameShower contactNameShower = new ContactNameShower(viewModel, model);
        contactNameShower.resume();

        assertThat(viewModel.getUserName(), is(name));
    }
}