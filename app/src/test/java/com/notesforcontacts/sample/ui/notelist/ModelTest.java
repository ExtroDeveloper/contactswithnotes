package com.notesforcontacts.sample.ui.notelist;

import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.Storages.ContactInfoFetcher;
import com.notesforcontacts.sample.Storages.NoteStorage;
import com.notesforcontacts.sample.Storages.NoteStorageUpdatedEvent;
import com.notesforcontacts.sample.Utilits.EventManager;
import com.notesforcontacts.sample.contacts.ContactInfo;
import com.notesforcontacts.sample.ui.notelist.model.Model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Subscription;
import rx.functions.Action1;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(RobolectricTestRunner.class)
public class ModelTest
{
    @Test
    public void testGetContactNameReturnsDataFromContactInfoFetcher() throws Exception
    {
        ContactInfoFetcher contactInfoFetcher = mock(ContactInfoFetcher.class);
        NoteStorage noteStorage = mock(NoteStorage.class);

        List<ContactInfo> list = new ArrayList<>();

        list.add(new ContactInfo("1", Collections.emptyList(), "11", 0, null));
        list.add(new ContactInfo("2", Collections.emptyList(), "22", 0, null));
        list.add(new ContactInfo("3", Collections.emptyList(), "33", 0, null));
        list.add(new ContactInfo("4", Collections.emptyList(), "44", 0, null));

        when(contactInfoFetcher.getContactInfoList()).thenAnswer(invocation -> list);

        for (int i = 0; i < list.size(); ++i)
        {
            Model model = Model.getNewInstanceForAllNotes(list.get(i).id, contactInfoFetcher, noteStorage);

            assertThat(model.getContactName(), is(list.get(i).name));
        }
    }

    @Test
    @Config(sdk = 23)
    public void testActionIsBeingCalledProperTimeOnSubscriptionAndAfterUnsubscription() throws Exception
    {
        NoteStorage noteStorageMock = mock(NoteStorage.class);

        Model model = Model.getNewInstanceForAllNotes("1", mock(ContactInfoFetcher.class), noteStorageMock);

        Action1<List<NoteForContact>> action1 = (Action1<List<NoteForContact>>) mock(Action1.class);

        Subscription subscription = model.subscribeToNoteList(action1);

        verify(action1, times(1)).call(any());

        EventManager.postEvent(new NoteStorageUpdatedEvent());

        verify(action1, times(2)).call(any());

        subscription.unsubscribe();

        EventManager.postEvent(new NoteStorageUpdatedEvent());

        verify(action1, times(2)).call(any());
    }

    @Test
    @Config(sdk = 23)
    public void testGetNoteText() throws Exception
    {
        NoteStorage noteStorageMock = mock(NoteStorage.class);

        String contactId = "1";
        Model model = Model.getNewInstanceForAllNotes(contactId, mock(ContactInfoFetcher.class), noteStorageMock);

        List<NoteForContact> list = new ArrayList<>();

        String text1 = "11";
        list.add(new NoteForContact(1, text1, true));
        String text2 = "22";
        list.add(new NoteForContact(2, text2, false));

        when(noteStorageMock.getNotesWithoutCheckedForContactId(contactId)).thenAnswer(invocation -> list);
        when(noteStorageMock.getAllNotesForContactId(contactId)).thenAnswer(invocation -> list);

        assertThat(model.getNoteText(1), is(text1));
        assertThat(model.getNoteText(2), is(text2));
    }

    @Test
    @Config(sdk = 23)
    public void testDeleteNote() throws Exception
    {
        NoteStorage noteStorageMock = mock(NoteStorage.class);

        Model model = Model.getNewInstanceForAllNotes("1", mock(ContactInfoFetcher.class), noteStorageMock);

        model.deleteNote(1);
        verify(noteStorageMock, Mockito.times(1)).deleteNote(1);

        model.deleteNote(2);
        verify(noteStorageMock, Mockito.times(1)).deleteNote(2);
    }

    @Test
    @Config(sdk = 23)
    public void testInvertCheckedState() throws Exception
    {
        NoteStorage noteStorageMock = mock(NoteStorage.class);

        List<NoteForContact> list = new ArrayList<>();

        boolean note1checked = false;
        boolean note2checked = false;
        int note1id = 1;
        int note2id = 2;
        list.add(new NoteForContact(note1id, "", note1checked));
        list.add(new NoteForContact(note2id, "", note2checked));

        String contactId = "1";
        when(noteStorageMock.getNotesWithoutCheckedForContactId(contactId)).thenReturn(list);

        when(noteStorageMock.getAllNotesForContactId(contactId)).thenReturn(list);

        Model model = Model.getNewInstanceForAllNotes(contactId, mock(ContactInfoFetcher.class), noteStorageMock);

        model.invertCheckedState(1);
        verify(noteStorageMock, Mockito.times(1)).updateNoteCheckedState(1, !note1checked);

        model.invertCheckedState(2);
        verify(noteStorageMock, Mockito.times(1)).updateNoteCheckedState(2, !note1checked);
    }
}