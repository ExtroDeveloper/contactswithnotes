package com.notesforcontacts.sample.ui.notelist.features;

import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.ui.notelist.ViewModel;
import com.notesforcontacts.sample.ui.notelist.model.Model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.functions.Action1;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(RobolectricTestRunner.class)
public class NoteListShowerTest
{
    @Test
    @Config(sdk = 23)
    public void testRunsOnceSetNotesOfViewModelWithDataFromModel() throws Exception
    {
        Model model = mock(Model.class);

        Subscription subscription = mock(Subscription.class);
        when(model.subscribeToNoteList(any())).thenReturn(subscription);

        NoteListShower noteListShower = new NoteListShower(mock(ViewModel.class), model);

        noteListShower.resume();

        verify(model, times(1)).subscribeToNoteList(any());

        noteListShower.stop();

        verify(subscription, times(1)).unsubscribe();
    }

    @Test
    @Config(sdk = 23)
    public void testNotesIsShownInViewModel() throws Exception
    {
        Model model = mock(Model.class);

        List<NoteForContact> notes = new ArrayList<>();

        ViewModel viewModel = mock(ViewModel.class);

        NoteListShower noteListShower = new NoteListShower(viewModel, model);

        when(model.subscribeToNoteList(any())).thenAnswer(new Answer<Subscription>()
        {
            @Override
            public Subscription answer(InvocationOnMock invocation) throws Throwable
            {
                Object[] args = invocation.getArguments();

                Action1<List<NoteForContact>> arg = (Action1<List<NoteForContact>>) args[0];
                arg.call(notes);

                return null;
            }
        });
        noteListShower.resume();

        verify(viewModel, times(1)).setNotes(notes);
    }
}