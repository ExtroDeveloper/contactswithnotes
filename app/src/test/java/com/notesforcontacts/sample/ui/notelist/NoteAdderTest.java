package com.notesforcontacts.sample.ui.notelist;

import com.notesforcontacts.sample.ui.notelist.features.EditNoteDialogShower;
import com.notesforcontacts.sample.ui.notelist.features.FragmentEditNoteDialogShower;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class NoteAdderTest
{
    @Test
    public void addNewNote() throws Exception
    {
        EditNoteDialogShower editNoteDialogShowerMock = mock(FragmentEditNoteDialogShower.class);
        NoteAdder noteAdder = new NoteAdder(editNoteDialogShowerMock);
        noteAdder.addNewNote();
        verify(editNoteDialogShowerMock, times(1)).showAddNoteEditDialog();
    }
}