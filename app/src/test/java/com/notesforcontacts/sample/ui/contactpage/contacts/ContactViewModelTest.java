package com.notesforcontacts.sample.ui.contactpage.contacts;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ContactViewModelTest
{
    @Test
    public void onClick() throws Exception
    {
        ContactViewModel.Callback callbackMock = mock(ContactViewModel.Callback.class);
        String contactId = "11";
        ContactViewModel viewModel = new ContactViewModel(contactId, "", "", null, callbackMock);

        viewModel.onClick();

        verify(callbackMock, times(1)).onClick();
    }
}