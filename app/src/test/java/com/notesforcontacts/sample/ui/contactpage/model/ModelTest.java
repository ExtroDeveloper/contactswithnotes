package com.notesforcontacts.sample.ui.contactpage.model;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.Storages.ContactInfoFetcher;
import com.notesforcontacts.sample.Storages.FilterStorage;
import com.notesforcontacts.sample.Storages.NoteStorage;
import com.notesforcontacts.sample.Storages.NoteStorageUpdatedEvent;
import com.notesforcontacts.sample.Utilits.EventManager;
import com.notesforcontacts.sample.contacts.ContactInfo;
import com.notesforcontacts.sample.contacts.FilterByNameHasNote;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.observers.TestSubscriber;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class ModelTest
{
    @Test
    @Config(sdk = 23)
    public void observableFetchesDataFromStorage() throws Exception
    {
        NoteStorage noteStorageMock = mock(NoteStorage.class);

        Map<String, List<NoteForContact>> map = getNotesById();

        when(noteStorageMock.getNotesByContactIds()).thenReturn(map);

        FilterStorage filterStorageMock = mock(FilterStorage.class);
        Observable<FilterByNameHasNote> name = Observable.just(new FilterByNameHasNote("", false));
        when(filterStorageMock.getObservable()).thenReturn(name);

        Model model = new Model(getContactInfoFetcherMock(), filterStorageMock, noteStorageMock);

        Observable<List<ContactInfo>> obs = model.getObservableContactInfoList();

        TestSubscriber<List<ContactInfo>> testSubscriber = new TestSubscriber<>();
        obs.subscribe(testSubscriber);

        List<ContactInfo> contactInfos = testSubscriber.getOnNextEvents().get(0);

        assertThat(contactInfos.get(0).noteCount, is(2));
        assertThat(contactInfos.size(), is(4));

        when(noteStorageMock.getNotesByContactIds()).thenReturn(getUpdatedNotesById());

        EventManager.postEvent(new NoteStorageUpdatedEvent());

        contactInfos = testSubscriber.getOnNextEvents().get(0);

        assertThat(contactInfos.get(0).noteCount, is(3));
    }

    private Map<String, List<NoteForContact>> getUpdatedNotesById()
    {
        Map<String, List<NoteForContact>> map = new ArrayMap<>();

        {
            List<NoteForContact> list = new ArrayList<>();
            String text1 = "11";
            list.add(new NoteForContact(1, text1, true));
            String text2 = "22";
            list.add(new NoteForContact(2, text2, false));

            String text3 = "333";
            list.add(new NoteForContact(3, text3, false));

            map.put("11", list);
        }

        {
            List<NoteForContact> list = new ArrayList<>();
            String text1 = "11";
            list.add(new NoteForContact(1, text1, true));
            String text2 = "22";
            list.add(new NoteForContact(2, text2, false));

            map.put("22", list);
        }

        map.put("name3", Collections.emptyList());
        return map;
    }

    @NonNull
    private Map<String, List<NoteForContact>> getNotesById()
    {
        Map<String, List<NoteForContact>> map = new ArrayMap<>();

        {
            List<NoteForContact> list = new ArrayList<>();
            String text1 = "11";
            list.add(new NoteForContact(1, text1, true));
            String text2 = "22";
            list.add(new NoteForContact(2, text2, false));

            map.put("11", list);
        }

        {
            List<NoteForContact> list = new ArrayList<>();
            String text1 = "11";
            list.add(new NoteForContact(1, text1, true));
            String text2 = "22";
            list.add(new NoteForContact(2, text2, false));

            map.put("22", list);
        }

        map.put("name3", Collections.emptyList());
        return map;
    }

    @NonNull
    private ContactInfoFetcher getContactInfoFetcherMock()
    {
        ContactInfoFetcher contactInfoFetcher = mock(ContactInfoFetcher.class);

        List<ContactInfo> list = new ArrayList<>();

        list.add(new ContactInfo("11", Collections.emptyList(), "name1", 0, null));
        list.add(new ContactInfo("22", Collections.emptyList(), "name2", 0, null));
        list.add(new ContactInfo("33", Collections.emptyList(), "name3", 0, null));
        list.add(new ContactInfo("44", Collections.emptyList(), "aaaaa", 0, null));

        when(contactInfoFetcher.getContactInfoList()).thenReturn(list);
        return contactInfoFetcher;
    }

}