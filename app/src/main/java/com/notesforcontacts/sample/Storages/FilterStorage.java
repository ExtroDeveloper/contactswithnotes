package com.notesforcontacts.sample.Storages;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.NotesForContactsApplication;
import com.notesforcontacts.sample.contacts.FilterByNameHasNote;

import rx.Observable;
import rx.subjects.BehaviorSubject;

public class FilterStorage
{
    public Observable<FilterByNameHasNote> getObservable()
    {
        if (subject == null)
        {
            subject = BehaviorSubject.create(SettingsStorage.getFilter(NotesForContactsApplication.application));
            subject.onNext(SettingsStorage.getFilter(NotesForContactsApplication.application));
        }

        return subject;
    }

    public void setFilter(@NonNull FilterByNameHasNote filter)
    {
        SettingsStorage.setFilter(NotesForContactsApplication.application, filter);
        subject.onNext(filter);
    }

    private static BehaviorSubject<FilterByNameHasNote> subject;
}