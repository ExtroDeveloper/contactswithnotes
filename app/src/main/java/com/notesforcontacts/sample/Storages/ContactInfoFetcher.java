package com.notesforcontacts.sample.Storages;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.NotesForContactsApplication;
import com.notesforcontacts.sample.contacts.ContactInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ContactInfoFetcher
{
    public static Pair<String, String> getContactIdAndNameByNumber(@NonNull Context context, @NonNull String number)
    {
        Cursor cursor = getCursor(context, number);

        try
        {
            if (cursor != null && cursor.getCount() > 0)
            {
                cursor.moveToFirst();

                return new Pair<>(getContactId(cursor), getContactName(cursor));
            }
        }
        finally
        {
            if (cursor != null)
            {
                cursor.close();
            }
        }

        return null;
    }

    public List<ContactInfo> getContactInfoList()
    {
        Cursor cursor = getCursor(NotesForContactsApplication.application);

        Map<String, ContactInfo> contacts = new HashMap<>();

        if (cursor == null)
        {
            return Collections.emptyList();
        }

        while (cursor.moveToNext())
        {
            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            ContactInfo contactInfo = contacts.get(id);

            if (contactInfo == null)
            {
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                Uri uri = getUri(cursor);
                contactInfo = new ContactInfo(id, new LinkedList<>(), name, 0, uri);
            }

            String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            contactInfo.phoneNumbers.add(number);
            contacts.put(id, contactInfo);
        }

        cursor.close();

        List<ContactInfo> result = new ArrayList<>(contacts.size());

        Map<String, List<NoteForContact>> notesForAll = new NoteStorage().getNotesByContactIds();

        for (String id : contacts.keySet())
        {
            int notesNumber = 0;

            List<NoteForContact> notes = notesForAll.get(id);

            if (notes != null)
            {
                notesNumber = notes.size();
            }

            ContactInfo contactInfo = contacts.get(id);
            contactInfo.noteCount = notesNumber;

            result.add(contactInfo);
        }

        return result;
    }

    private static Cursor getCursor(@NonNull Context context)
    {
        ContentResolver contentResolver = context.getContentResolver();

        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC";
        return contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, sortOrder);
    }

    private static String getContactId(@NonNull Cursor cursor)
    {
        return cursor.getString(cursor.getColumnIndex(BaseColumns._ID));
    }

    private static String getContactName(@NonNull Cursor cursor)
    {
        return cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
    }

    private static Cursor getCursor(@NonNull Context context, @NonNull String number)
    {
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        return contentResolver.query(uri, new String[]{BaseColumns._ID, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME}, null, null, null);
    }

    private Uri getUri(@NonNull Cursor cursor)
    {
        String string = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

        return string != null ? Uri.parse(string) : Uri.EMPTY;
    }
}