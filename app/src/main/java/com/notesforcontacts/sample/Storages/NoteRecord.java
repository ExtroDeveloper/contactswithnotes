package com.notesforcontacts.sample.Storages;

import android.support.annotation.NonNull;

import com.orm.SugarRecord;

public class NoteRecord extends SugarRecord
{
    public static NoteRecord newInstance(@NonNull String contactId, @NonNull String text, boolean checked)
    {
        NoteRecord record = new NoteRecord();
        record.contactId = contactId;
        record.checked = checked;
        record.text = text;

        return record;
    }

    public String contactId;
    public boolean checked;
    public String text;
}