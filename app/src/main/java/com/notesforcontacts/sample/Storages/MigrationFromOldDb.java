package com.notesforcontacts.sample.Storages;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

public class MigrationFromOldDb
{
    public static void migrateToORM(@NonNull Context context)
    {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursor = db.query("mytable", null, null, null, null, null, null);

        int contactIdIndex = cursor.getColumnIndex("contactId");
        int noteColIndex = cursor.getColumnIndex("note");
        int checkedColIndex = cursor.getColumnIndex("checked");

        if (cursor.moveToFirst())
        {
            do
            {
                String contactId = cursor.getString(contactIdIndex);
                String text = cursor.getString(noteColIndex);
                boolean checked = cursor.getInt(checkedColIndex) == 1;

                NoteRecord record = NoteRecord.newInstance(contactId, text, checked);
                record.save();
            }
            while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
    }

    private static class DBHelper extends SQLiteOpenHelper
    {
        DBHelper(Context context)
        {
            super(context, "myDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL("create table mytable ("
                    + "id integer primary key autoincrement,"
                    + "contactId sourceText,"
                    + "note sourceText,"
                    + "checked int" + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
        }
    }
}