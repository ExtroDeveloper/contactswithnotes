package com.notesforcontacts.sample.Storages;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.Utilits.EventManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class NoteStorage
{
    public void updateNoteText(long noteId, @NonNull String text)
    {
        NoteRecord record = NoteRecord.findById(NoteRecord.class, noteId);
        record.text = text;
        record.save();

        EventManager.getInstance().post(new NoteStorageUpdatedEvent());
    }

    public void addNote(@NonNull String contactId, @NonNull String text)
    {
        NoteRecord record = NoteRecord.newInstance(contactId, text, false);
        record.text = text;
        record.save();

        EventManager.getInstance().post(new NoteStorageUpdatedEvent());
    }

    public void deleteNote(long noteId)
    {
        NoteRecord record = NoteRecord.findById(NoteRecord.class, noteId);
        record.delete();

        EventManager.getInstance().post(new NoteStorageUpdatedEvent());
    }

    public void updateNoteCheckedState(long noteId, boolean checked)
    {
        NoteRecord record = NoteRecord.findById(NoteRecord.class, noteId);
        record.checked = checked;
        record.save();

        EventManager.getInstance().post(new NoteStorageUpdatedEvent());
    }

    public List<NoteForContact> getAllNotesForContactId(@NonNull String contactId)
    {
        return getNotes(contactId, false);
    }

    public List<NoteForContact> getNotesWithoutCheckedForContactId(@NonNull String contactId)
    {
        return getNotes(contactId, true);
    }

    public Map<String, List<NoteForContact>> getNotesByContactIds()
    {
        List<NoteRecord> records = NoteRecord.find(NoteRecord.class, null);

        Map<String, List<NoteForContact>> result = new HashMap<>();

        for (NoteRecord record : records)
        {
            List<NoteForContact> notes = result.get(record.contactId);

            if (notes == null)
            {
                notes = new LinkedList<>();
            }
            notes.add(new NoteForContact(record.getId(), record.text, record.checked));
            result.put(record.contactId, notes);
        }

        return result;
    }

    public int getNotesCount(@NonNull String contactId, boolean excludeChecked)
    {
        if (excludeChecked)
        {
            return (int) NoteRecord.count(NoteRecord.class, "contact_Id = ? and checked <> ?", new String[]{contactId, "1"});
        }
        else
        {
            return (int) NoteRecord.count(NoteRecord.class, "contact_Id = ?", new String[]{contactId});
        }
    }

    private List<NoteForContact> getNotes(@NonNull String contactId, boolean excludeChecked)
    {
        List<NoteRecord> records;
        if (excludeChecked)
        {
            records = NoteRecord.find(NoteRecord.class, "contact_Id = ? and checked <> ?", contactId, "1");
        }
        else
        {
            records = NoteRecord.find(NoteRecord.class, "contact_Id = ?", contactId);
        }

        List<NoteForContact> notes = new ArrayList<>();

        for (NoteRecord record : records)
        {
            notes.add(new NoteForContact(record.getId(), record.text, record.checked));
        }

        return notes;
    }
}