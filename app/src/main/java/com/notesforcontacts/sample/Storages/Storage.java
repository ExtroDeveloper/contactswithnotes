package com.notesforcontacts.sample.Storages;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

public class Storage
{
    public static int loadInt(@NonNull Context context, @NonNull String name, @SuppressWarnings("SameParameterValue") int defaultVal)
    {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getInt(name, defaultVal);
    }

    public static void saveBoolean(@NonNull Context context, @NonNull String name, Boolean val)
    {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(name, val);
        editor.commit();
    }

    public static Boolean loadBoolean(@NonNull Context context, @NonNull String name, @NonNull Boolean defaultVal)
    {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(name, defaultVal);
    }

    public static void saveString(@NonNull Context context, @NonNull String name, @NonNull String val)
    {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putString(name, val);
        editor.commit();
    }

    public static String loadString(@NonNull Context context, @NonNull String name, @NonNull String defaultVal)
    {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(name, defaultVal);
    }

    private static SharedPreferences.Editor getEditor(@NonNull Context context)
    {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.edit();
    }
}