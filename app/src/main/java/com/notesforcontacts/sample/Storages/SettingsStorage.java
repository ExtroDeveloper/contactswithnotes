package com.notesforcontacts.sample.Storages;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.util.Log;

import com.notesforcontacts.sample.R;
import com.notesforcontacts.sample.Utilits.PopupPosition;
import com.notesforcontacts.sample.contacts.FilterByNameHasNote;

import java.util.Locale;

public class SettingsStorage
{
    public static void setLocale(@NonNull Context context, @NonNull String loc)
    {
        Locale locale = new Locale(loc);
        Locale.setDefault(locale);
        Configuration config = context.getApplicationContext().getResources().getConfiguration();
        config.locale = locale;
        context.getApplicationContext().getResources().updateConfiguration(config,
                context.getApplicationContext().getResources().getDisplayMetrics());

        Storage.saveString(context, smLocal, loc);
    }

    public static FilterByNameHasNote getFilter(@NonNull Context context)
    {
        boolean onlyWithNotes = Storage.loadBoolean(context, FILTER_ONLY_WITH_NOTE, false);
        String name = Storage.loadString(context, FILTER_NAME, "");

        return new FilterByNameHasNote(name, onlyWithNotes);
    }

    public static void setFilter(@NonNull Context context, @NonNull FilterByNameHasNote filter)
    {
        Storage.saveBoolean(context, FILTER_ONLY_WITH_NOTE, filter.isOnlyWithNotes());
        Storage.saveString(context, FILTER_NAME, filter.getName());
    }

    public static int getContactInfoSmallHeight(@NonNull Context context)
    {
        String floatingNoteListHeight = "FloatingNoteListHeight";
        return Storage.loadInt(context, floatingNoteListHeight, 50);
    }

    public static boolean getShowCheckedNotesInWindow(@NonNull Context context)
    {
        String smShowCheckedNotesInWindow = "showChecked";
        return Storage.loadBoolean(context, smShowCheckedNotesInWindow, false);
    }

    public static boolean getShowWindowOnCallIfNoNotes(@NonNull Context context, boolean incoming)
    {
        int res = incoming ? R.string.showIfNoNotes_incoming_key : R.string.showIfNoNotes_outgoing_key;
        String string = context.getString(res);

        return Storage.loadBoolean(context, string, false);
    }

    public static boolean needShowPopup(@NonNull Context context, boolean incoming)
    {
        int res = incoming ? R.string.floating_window_gravity_on_incoming_key : R.string.floating_window_gravity_on_outgoing_key;
        String defaultValue = context.getString(R.string.floating_window_gravity_default);

        String value = Storage.loadString(context, context.getString(res), defaultValue);
        Log.d("DEBUG", "needShowPopup = " + res);
        Log.d("DEBUG", "value = " + value);

        return !value.equals("Don_t_show");
    }

    public static PopupPosition getPositionFowWindowOnIncomingTop(@NonNull Context context)
    {
        String defaultValue = context.getString(R.string.floating_window_gravity_default);
        String value = Storage.loadString(context, context.getString(R.string.floating_window_gravity_on_incoming_key), defaultValue);

        if (value.equals("top"))
        {
            return PopupPosition.TOP;
        }
        else if (value.equals("bottom"))
        {
            return PopupPosition.BOTTOM;
        }

        return PopupPosition.DONT_SHOW;
    }

    public static PopupPosition getPositionFowWindowOnOutgoingTop(@NonNull Context context)
    {
        String defaultValue = context.getString(R.string.floating_window_gravity_default);
        String value = Storage.loadString(context, context.getString(R.string.floating_window_gravity_on_outgoing_key), defaultValue);
        Log.d("DEBUG", "getPositionFowWindowOnOutgoingTop value = " + value);

        if (value.equals("top"))
        {
            return PopupPosition.TOP;
        }
        else if (value.equals("bottom"))
        {
            return PopupPosition.BOTTOM;
        }

        return PopupPosition.DONT_SHOW;
    }

    private static final String smLocal = "local";
    private static final String FILTER_ONLY_WITH_NOTE = "FILTER_ONLY_WITH_NOTE";
    private static final String FILTER_NAME = "FILTER_NAME";
}