package com.notesforcontacts.sample.ui.notelist.note;

import android.os.Bundle;

import com.android.annotations.NonNull;
import com.android.annotations.Nullable;
import com.notesforcontacts.sample.ui.common.BaseDiffCallback;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModel;

import java.util.List;

public class NoteItemDiffCallback extends BaseDiffCallback<NoteViewModel>
{
    public NoteItemDiffCallback(@NonNull List<NoteViewModel> oldList, @NonNull List<NoteViewModel> newList)
    {
        super(oldList, newList);
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition)
    {
        return oldList.get(oldItemPosition).getId() == newList.get(newItemPosition).getId();
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition)
    {
        NoteViewModel newProduct = newList.get(newItemPosition);
        NoteViewModel oldProduct = oldList.get(oldItemPosition);

        Bundle diffBundle = new Bundle();

        if (!newProduct.getText().equals(oldProduct.getText()))
        {
            diffBundle.putString("text", newProduct.getText());
        }
        if (newProduct.isChecked() != oldProduct.isChecked())
        {
            diffBundle.putBoolean("checked", newProduct.isChecked());
        }

        return diffBundle;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition)
    {
        NoteViewModel oldItem = oldList.get(oldItemPosition);
        NoteViewModel newItem = newList.get(newItemPosition);
        return oldItem.getText().equals(newItem.getText())
                && oldItem.isChecked() == newItem.isChecked();
    }
}