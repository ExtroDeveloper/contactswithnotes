package com.notesforcontacts.sample.ui.contactpage;

import android.support.annotation.NonNull;

public class ContactClickEvent
{
    public ContactClickEvent(@NonNull String contactId)
    {
        this.contactId = contactId;
    }

    public final String contactId;

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }

        if (getClass() != obj.getClass())
        {
            return false;
        }

        ContactClickEvent toCompare = this.getClass().cast(obj);

        return contactId.equals(toCompare.contactId);
    }
}
