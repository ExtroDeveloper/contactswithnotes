package com.notesforcontacts.sample.ui.notelist.note;

import android.support.annotation.NonNull;

public interface LongClickAction
{
    class NoAction implements LongClickAction
    {

        @Override
        public void run(@NonNull String text)
        {
        }
    }

    void run(@NonNull String text);
}
