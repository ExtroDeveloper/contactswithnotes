package com.notesforcontacts.sample.ui.notelist.note;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.notesforcontacts.sample.BR;

public class NoteViewModel extends BaseObservable
{
    public interface Callback
    {
        void onTextPressed();

        void onDeleteNoteButton();

        void onEditNoteButton(String text);

        void onLongClick(String text);
    }

    public NoteViewModel(long id, Callback callback)
    {
        this.id = id;
        this.callback = callback;
    }

    public void onTextClick()
    {
        callback.onTextPressed();
    }

    public void onDeleteClick()
    {
        callback.onDeleteNoteButton();
    }

    public void onEditClick()
    {
        callback.onEditNoteButton(text);
    }

    public void onLongClick()
    {
        callback.onLongClick(text);
    }

    @Bindable
    public boolean isChecked()
    {
        return checked;
    }

    public void setChecked(boolean checked)
    {
        this.checked = checked;
        notifyPropertyChanged(BR.checked);
        notifyPropertyChanged(BR.text);
    }

    @Bindable
    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
        notifyPropertyChanged(BR.text);
    }

    public long getId()
    {
        return id;
    }

    private final Callback callback;
    private final long id;
    private boolean checked;
    private String text = "";
}