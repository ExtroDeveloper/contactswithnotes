package com.notesforcontacts.sample.ui.notelist.features;

import android.support.v4.app.Fragment;

import com.notesforcontacts.sample.ui.common.editnotedialog.Params;

public class FragmentEditNoteDialogShower implements EditNoteDialogShower
{
    public FragmentEditNoteDialogShower(Fragment fragment)
    {
        this.fragment = fragment;
    }

    @Override
    public void showNoteEditDialog(long noteId, String text)
    {
        Params params = new Params(noteId, text);
        new EditNoteDialogBuilder(params).build().show(fragment.getFragmentManager(), null);
    }

    @Override
    public void showAddNoteEditDialog()
    {
        showNoteEditDialog(Params.noNoteId, "");
    }

    private final Fragment fragment;
}