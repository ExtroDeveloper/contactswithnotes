package com.notesforcontacts.sample.ui.notelist.note;

import com.notesforcontacts.sample.ui.notelist.model.Model;

public class NoteCheckedStateInverter
{
    public NoteCheckedStateInverter(Model model)
    {
        this.model = model;
    }

    public void invertCheckedState(long id)
    {
        model.invertCheckedState(id);
    }

    private final Model model;
}