package com.notesforcontacts.sample.ui.contactpage.filter;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.notesforcontacts.sample.R;
import com.notesforcontacts.sample.Storages.FilterStorage;
import com.notesforcontacts.sample.contacts.FilterByNameHasNote;
import com.notesforcontacts.sample.databinding.FilterDialogFragmentBinding;

public class FilterDialogFragment extends DialogFragment
{
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        FilterDialogFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.filter_dialog_fragment, container, false);

        FilterByNameHasNote filterByNameHasNote = new FilterStorage().getObservable().toBlocking().first();

        ViewModel viewModel = new ViewModel(filterByNameHasNote.getName(), filterByNameHasNote.isOnlyWithNotes());
        viewModel.setActionOnOkClick(() -> getDialog().dismiss());

        viewModel.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback()
        {
            @Override
            public void onPropertyChanged(Observable observable, int i)
            {
                FilterByNameHasNote newFilterByNameHasNote = new FilterByNameHasNote(viewModel.getNameOrPhone(), viewModel.isOnlyWithNotes());
                new FilterStorage().setFilter(newFilterByNameHasNote);
            }
        });

        binding.setViewModel(viewModel);
        binding.setCancelClick(v -> getDialog().dismiss());
        binding.namePhone.setOnFocusChangeListener((v, hasFocus) ->
        {
            if (hasFocus)
            {
                getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });

        return binding.getRoot();
    }

}