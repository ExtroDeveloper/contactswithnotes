package com.notesforcontacts.sample.ui.contactpage;

import com.notesforcontacts.sample.ui.common.Feature;
import com.notesforcontacts.sample.ui.contactpage.model.Model;

import rx.Subscription;

public class ContactListShower extends Feature
{
    public ContactListShower(ViewModel viewModel, Model model)
    {
        this.viewModel = viewModel;
        this.model = model;
    }

    @Override
    public void stop()
    {
        subscription.unsubscribe();
    }

    @Override
    public void resume()
    {
        subscription = model.getObservableContactInfoList().subscribe(viewModel::setContactInfos);
    }

    private final ViewModel viewModel;
    private final Model model;
    private Subscription subscription;
}
