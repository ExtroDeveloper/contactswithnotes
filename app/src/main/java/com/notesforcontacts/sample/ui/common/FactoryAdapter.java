package com.notesforcontacts.sample.ui.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup;

@SuppressWarnings("unused")
public class FactoryAdapter<M> extends RecyclerArrayAdapter<M>
{
    public FactoryAdapter(@NonNull Context context, @NonNull HolderFactory<M> holderFactory)
    {
        super(context);
        this.holderFactory = holderFactory;
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
    {
        return holderFactory.buildHolder(parent);
    }

    public void addItem(M object)
    {
        getItems().add(object);
        notifyItemInserted(getItems().size() - 1);
    }

    public void updateItem(int position)
    {
        notifyItemChanged(position);
    }

    public void removeItem(int position)
    {
        remove(position);
        notifyItemRemoved(position);
    }

    private final HolderFactory<M> holderFactory;
}
