package com.notesforcontacts.sample.ui.notelist.features;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.notesforcontacts.sample.ui.common.editnotedialog.EditNoteDialogCreator;
import com.notesforcontacts.sample.ui.common.editnotedialog.Params;

@FragmentWithArgs
public class EditNoteDialog extends DialogFragment
{

    @Arg
    public Params params;

    @Override
    public void show(FragmentManager manager, String tag)
    {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FragmentArgs.inject(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Context context = getContext();

        return new EditNoteDialogCreator(context, params).create();
    }

}