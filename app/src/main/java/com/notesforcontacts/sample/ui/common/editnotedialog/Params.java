package com.notesforcontacts.sample.ui.common.editnotedialog;

import java.io.Serializable;

public class Params implements Serializable
{
    public static Params getParamsForNewNote()
    {
        return new Params(noNoteId, "");
    }

    public static final long noNoteId = -1;

    public Params(long noteId, String sourceText)
    {
        this.noteId = noteId;
        this.sourceText = sourceText;
    }

    public final long noteId;
    public String sourceText = "";

    @Override
    public boolean equals(Object obj)
    {
        if (obj == this)
        {
            return true;
        }

        if (obj == null)
        {
            return false;
        }

        if (getClass() != obj.getClass())
        {
            return false;
        }

        Params toCompare = (Params) obj;

        return noteId == toCompare.noteId
                && sourceText.equals(toCompare.sourceText);
    }
}
