package com.notesforcontacts.sample.ui.common;

import com.notesforcontacts.sample.Storages.NoteStorageUpdatedEvent;
import com.notesforcontacts.sample.Utilits.EventManager;
import com.squareup.otto.Subscribe;

import rx.Observable;
import rx.Subscriber;
import rx.android.MainThreadSubscription;

public class NoteStorageUpdateObserver implements Observable.OnSubscribe<Integer>
{
    @Override
    public void call(Subscriber<? super Integer> subscriber)
    {
        if (eventHandler == null)
        {
            eventHandler = new Object()
            {
                @Subscribe
                public void handel(NoteStorageUpdatedEvent event)
                {
                    subscriber.onNext(1);
                }
            };

            EventManager.register(eventHandler);
        }

        subscriber.add(new MainThreadSubscription()
        {
            @Override
            protected void onUnsubscribe()
            {
                EventManager.unregister(eventHandler);
            }
        });
        subscriber.onNext(1);
    }

    private Object eventHandler;
}
