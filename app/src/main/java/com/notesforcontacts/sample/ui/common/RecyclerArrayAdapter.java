package com.notesforcontacts.sample.ui.common;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unused", "UnusedParameters", "ConstantConditions"})
abstract public class RecyclerArrayAdapter<T> extends RecyclerView.Adapter<BaseViewHolder>
{
    public interface ItemView
    {
        View onCreateView(ViewGroup parent);

        void onBindView(View headerView);
    }

    public interface OnLoadMoreListener
    {
        void onLoadMore();
    }

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public interface OnItemLongClickListener
    {
        boolean onItemClick(int position);
    }

    public interface EventDelegate
    {
        void addData(int length);

        void clear();

        void stopLoadMore();

        void pauseLoadMore();

        void resumeLoadMore();

        void setMore(View view, OnLoadMoreListener listener);

        void setNoMore(View view);

        void setErrorMore(View view);
    }

    public RecyclerArrayAdapter(Context context)
    {
        init(context, new ArrayList<>());
    }

    public void add(T object)
    {
        if (mEventDelegate != null) mEventDelegate.addData(object == null ? 0 : 1);
        if (object != null)
        {
            synchronized (mLock)
            {
                mObjects.add(object);
            }
        }

        if (mNotifyOnChange)
        {
            notifyItemChanged(getCount());
        }
    }

    public void addAll(Collection<? extends T> collection)
    {
        if (mEventDelegate != null)
            mEventDelegate.addData(collection == null ? 0 : collection.size());
        if (collection != null && collection.size() != 0)
        {
            synchronized (mLock)
            {
                mObjects.addAll(collection);
            }
        }
        if (mNotifyOnChange) notifyDataSetChanged();
    }

    public void setWithNoUpdate(Collection<? extends T> collection)
    {
        if (mEventDelegate != null)
        {
            mEventDelegate.addData(collection == null ? 0 : collection.size());
        }
        if (collection != null)
        {
            synchronized (mLock)
            {
                mObjects.clear();
                mObjects.addAll(collection);
            }
        }
    }

    public void set(Collection<? extends T> collection)
    {
        if (mEventDelegate != null)
        {
            mEventDelegate.addData(collection == null ? 0 : collection.size());
        }
        if (collection != null)
        {
            synchronized (mLock)
            {
                mObjects.clear();
                mObjects.addAll(collection);
            }
            if (mNotifyOnChange)
            {
                notifyDataSetChanged();
            }
        }
    }

    public void addAll(T[] items)
    {
        if (mEventDelegate != null) mEventDelegate.addData(items == null ? 0 : items.length);
        if (items != null && items.length != 0)
        {
            synchronized (mLock)
            {
                Collections.addAll(mObjects, items);
            }
        }
        if (mNotifyOnChange) notifyDataSetChanged();
    }

    public void addFooter(ItemView view)
    {
        if (view == null) throw new NullPointerException("ItemView can't be null");
        footers.add(view);
    }

    public void remove(T object)
    {
        synchronized (mLock)
        {
            mObjects.remove(object);
        }
        if (mNotifyOnChange) notifyDataSetChanged();
    }

    public List<T> getItems()
    {
        return new ArrayList<>(mObjects);
    }

    public void remove(int position)
    {
        synchronized (mLock)
        {
            mObjects.remove(position);
        }
        if (mNotifyOnChange) notifyDataSetChanged();
    }

    public void clear()
    {
        if (mEventDelegate != null) mEventDelegate.clear();
        synchronized (mLock)
        {
            mObjects.clear();
        }
        if (mNotifyOnChange) notifyDataSetChanged();
    }

    public Context getContext()
    {
        return mContext;
    }

    public void setContext(Context ctx)
    {
        mContext = ctx;
    }

    public int getCount()
    {
        return mObjects.size();
    }

    public boolean isEmpty()
    {
        return mObjects.isEmpty();
    }

    @Override
    public final BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = createSpViewByType(parent, viewType);
        if (view != null)
        {
            return new StateViewHolder(view);
        }

        final BaseViewHolder viewHolder = OnCreateViewHolder(parent, viewType);

        if (mItemClickListener != null)
        {
            viewHolder.itemView.setOnClickListener(v -> mItemClickListener.onItemClick(viewHolder.getAdapterPosition() - headers.size()));
        }

        if (mItemLongClickListener != null)
        {
            viewHolder.itemView.setOnLongClickListener(v -> mItemLongClickListener.onItemClick(viewHolder.getAdapterPosition() - headers.size()));
        }
        return viewHolder;
    }

    @Override
    public final void onBindViewHolder(BaseViewHolder holder, int position)
    {
        holder.itemView.setId(position);
        if (headers.size() != 0 && position < headers.size())
        {
            headers.get(position).onBindView(holder.itemView);
            return;
        }

        int i = position - headers.size() - mObjects.size();
        if (footers.size() != 0 && i >= 0)
        {
            footers.get(i).onBindView(holder.itemView);
            return;
        }
        OnBindViewHolder(holder, position - headers.size());
    }

    @Deprecated
    @Override
    public final int getItemViewType(int position)
    {
        return 0;
    }

    public long getItemId(int position)
    {
        return position;
    }

    @Deprecated
    @Override
    public final int getItemCount()
    {
        return mObjects.size() + headers.size() + footers.size();
    }

    abstract public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType);

    public void OnBindViewHolder(BaseViewHolder holder, final int position)
    {
        //noinspection unchecked
        holder.setData(getItem(position));
    }

    public List<T> getAllData()
    {
        return new ArrayList<>(mObjects);
    }

    public T getItem(int position)
    {
        return mObjects.get(position);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        this.mItemClickListener = listener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener listener)
    {
        this.mItemLongClickListener = listener;
    }
    protected final ArrayList<ItemView> headers = new ArrayList<>();
    protected final ArrayList<ItemView> footers = new ArrayList<>();
    private final Object mLock = new Object();
    private final boolean mNotifyOnChange = true;
    protected List<T> mObjects;
    protected EventDelegate mEventDelegate;
    protected OnItemClickListener mItemClickListener;
    protected OnItemLongClickListener mItemLongClickListener;
    private Context mContext;

    private void init(Context context, List<T> objects)
    {
        mContext = context;
        mObjects = objects;
    }

    private View createSpViewByType(ViewGroup parent, int viewType)
    {
        for (ItemView headerView : headers)
        {
            if (headerView.hashCode() == viewType)
            {
                View view = headerView.onCreateView(parent);
                StaggeredGridLayoutManager.LayoutParams layoutParams;
                if (view.getLayoutParams() != null)
                    layoutParams = new StaggeredGridLayoutManager.LayoutParams(view.getLayoutParams());
                else
                    layoutParams = new StaggeredGridLayoutManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setFullSpan(true);
                view.setLayoutParams(layoutParams);
                return view;
            }
        }
        for (ItemView itemView : footers)
        {
            if (itemView.hashCode() == viewType)
            {
                View view = itemView.onCreateView(parent);
                StaggeredGridLayoutManager.LayoutParams layoutParams;
                if (view.getLayoutParams() != null)
                    layoutParams = new StaggeredGridLayoutManager.LayoutParams(view.getLayoutParams());
                else
                    layoutParams = new StaggeredGridLayoutManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setFullSpan(true);
                view.setLayoutParams(layoutParams);
                return view;
            }
        }
        return null;
    }

    private class StateViewHolder extends BaseViewHolder
    {

        public StateViewHolder(View itemView)
        {
            super(itemView);
        }
    }
}