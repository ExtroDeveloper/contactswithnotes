package com.notesforcontacts.sample.ui.contactpage;

import android.content.res.TypedArray;
import android.databinding.Observable;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.notesforcontacts.sample.BR;
import com.notesforcontacts.sample.R;
import com.notesforcontacts.sample.Storages.ContactInfoFetcher;
import com.notesforcontacts.sample.Storages.FilterStorage;
import com.notesforcontacts.sample.Storages.NoteStorage;
import com.notesforcontacts.sample.databinding.ContactListBinding;
import com.notesforcontacts.sample.ui.common.Feature;
import com.notesforcontacts.sample.ui.contactpage.filter.FilterDialogFragment;
import com.notesforcontacts.sample.ui.contactpage.model.Model;

import java.util.ArrayList;
import java.util.List;

import rx.subjects.BehaviorSubject;

public class ContactListFragment extends Fragment
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initModel();
        initViewModel();
        initFeatures();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ContactListBinding binding = ContactListBinding.inflate(inflater, container, false);
        binding.setViewModel(viewModel);
        binding.executePendingBindings();

        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.toolbar);

        setHasOptionsMenu(true);

        binding.toolbar.setOnHierarchyChangeListener(new MenuHierarchyChangeListener());

        return binding.getRoot();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        for (Feature feature : featureList)
        {
            feature.resume();
        }

        viewModel.addOnPropertyChangedCallback(propertyChangedCallback);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        for (Feature feature : featureList)
        {
            feature.resume();
        }
        viewModel.removeOnPropertyChangedCallback(propertyChangedCallback);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        menu.clear();

        inflater.inflate(R.menu.menu_main, menu);

        actionFilter = menu.findItem(R.id.action_filter);
        Drawable settingsDrawable = actionFilter.getIcon();
        setTintColor(settingsDrawable);

        actionFilter.setIcon(settingsDrawable);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item == actionFilter)
        {
            FilterDialogFragment notesListInApp = new FilterDialogFragment();
            notesListInApp.show(getActivity().getSupportFragmentManager(), "");

            return true;
        }

        return false;
    }

    private final BehaviorSubject<Void> menuReady = BehaviorSubject.create();
    private final List<Feature> featureList = new ArrayList<>();
    private ViewModel viewModel;
    private Model model;
    private MenuItem actionFilter;
    private final Observable.OnPropertyChangedCallback propertyChangedCallback = new Observable.OnPropertyChangedCallback()
    {
        @Override
        public void onPropertyChanged(Observable observable, int i)
        {
            if (i == BR.highlightFilterIcon)
            {
                highlightFilterMenuItemView();
            }
        }
    };

    private void initFeatures()
    {
        featureList.add(new ContactListShower(viewModel, model));
        featureList.add(new FilterIconHighLighter(viewModel, new FilterStorage(), menuReady));
    }

    private void initViewModel()
    {
        NoteListPageOpenRequester noteListPageOpenRequester = new NoteListPageOpenRequester();
        ContactActionShower contactActionShower = new ContactActionShower(this);
        ContactViewModelFactory contactViewModelFactory = new ContactViewModelFactory(noteListPageOpenRequester, contactActionShower);
        viewModel = new ViewModel(contactViewModelFactory);
    }

    private void initModel()
    {
        model = new Model(new ContactInfoFetcher(), new FilterStorage(), new NoteStorage());
    }

    private void highlightFilterMenuItemView()
    {
        int itemId = actionFilter.getItemId();
        FragmentActivity activity = getActivity();
        View actionFilterView = activity.findViewById(itemId);

        if (actionFilterView == null)
        {
            return;
        }

        if (viewModel.isHighlightFilterIcon())
        {
            actionFilterView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.accent));
        }
        else
        {
            actionFilterView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void setTintColor(Drawable drawable)
    {
        TypedArray a = getContext().getTheme().obtainStyledAttributes(R.style.AppTheme, new int[]{R.attr.actionMenuTextColor});
        int attributeResourceId = a.getColor(0, 0);

        drawable.setColorFilter(attributeResourceId, PorterDuff.Mode.SRC_IN);
    }

    private class MenuItemHierarchyChangeListener implements ViewGroup.OnHierarchyChangeListener
    {

        @Override
        public void onChildViewAdded(View parent, View child)
        {
            if (child.getId() == actionFilter.getItemId())
            {
                menuReady.onNext(null);
            }
        }

        @Override
        public void onChildViewRemoved(View parent, View child)
        {
        }
    }

    private class MenuHierarchyChangeListener implements ViewGroup.OnHierarchyChangeListener
    {
        @Override
        public void onChildViewRemoved(View parent, View child)
        {
        }

        @Override
        public void onChildViewAdded(View parent, View child)
        {
            if (child instanceof ActionMenuView)
            {
                ActionMenuView view = (ActionMenuView) child;
                view.setOnHierarchyChangeListener(new MenuItemHierarchyChangeListener());
            }
        }
    }
}