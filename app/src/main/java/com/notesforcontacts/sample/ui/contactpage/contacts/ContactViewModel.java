package com.notesforcontacts.sample.ui.contactpage.contacts;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;
import android.support.annotation.NonNull;

public class ContactViewModel extends BaseObservable
{
    public interface Callback
    {
        void onClick();

        void onLongClick();
    }

    public ContactViewModel(@NonNull String name, @NonNull String phone, @NonNull String notesNumber, @NonNull Uri photoUri, @NonNull Callback callback)
    {
        this.name = name;
        this.phone = phone;
        this.notesNumber = notesNumber;
        this.photoUri = photoUri;
        this.callback = callback;
    }

    @Bindable
    public Uri getPhotoUri()
    {
        return photoUri;
    }

    public void onClick()
    {
        callback.onClick();
    }

    public void onLongClick()
    {
        callback.onLongClick();
    }

    @Bindable
    public String getName()
    {
        return name;
    }

    @Bindable
    public String getPhone()
    {
        return phone;
    }

    @Bindable
    public String getNotesNumber()
    {
        return notesNumber;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContactViewModel that = (ContactViewModel) o;
        return com.google.common.base.Objects.equal(callback, that.callback) &&
                com.google.common.base.Objects.equal(name, that.name) &&
                com.google.common.base.Objects.equal(phone, that.phone) &&
                com.google.common.base.Objects.equal(notesNumber, that.notesNumber);
    }

    private final Callback callback;
    private final String name;
    private final String phone;
    private final String notesNumber;
    private final Uri photoUri;
}
