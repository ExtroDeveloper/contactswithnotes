package com.notesforcontacts.sample.ui.notelist.note;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.ui.notelist.features.EditNoteDialogShower;

public class NoteViewModelFactory
{
    public NoteViewModelFactory(@NonNull EditNoteDialogShower editNoteDialogShower, @NonNull NoteDeleter noteDeleter, @NonNull NoteCheckedStateInverter noteCheckedStateInverter, LongClickAction longClickAction)
    {
        this.editNoteDialogShower = editNoteDialogShower;
        this.noteDeleter = noteDeleter;
        this.noteCheckedStateInverter = noteCheckedStateInverter;
        this.longClickAction = longClickAction;
    }

    @NonNull
    public NoteViewModel createViewModel(@NonNull NoteForContact noteForContact)
    {
        NoteViewModel.Callback callback = new NoteViewModelCallback(noteForContact.id, noteCheckedStateInverter, editNoteDialogShower, noteDeleter, longClickAction);
        NoteViewModel noteViewModel = new NoteViewModel(noteForContact.id, callback);
        noteViewModel.setText(noteForContact.text);
        noteViewModel.setChecked(noteForContact.checked);

        return noteViewModel;
    }

    private final NoteCheckedStateInverter noteCheckedStateInverter;
    private final EditNoteDialogShower editNoteDialogShower;
    private final NoteDeleter noteDeleter;
    private final LongClickAction longClickAction;
}