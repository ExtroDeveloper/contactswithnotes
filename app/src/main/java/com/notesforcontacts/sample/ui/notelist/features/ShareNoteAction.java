package com.notesforcontacts.sample.ui.notelist.features;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.notesforcontacts.sample.ui.notelist.note.LongClickAction;

public class ShareNoteAction implements LongClickAction
{
    public ShareNoteAction(@NonNull Context context)
    {
        this.context = context;
    }

    @Override
    public void run(@NonNull String text)
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
        Intent chooser = Intent.createChooser(sharingIntent, "Share via");
        chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(chooser);
    }

    private final Context context;
}
