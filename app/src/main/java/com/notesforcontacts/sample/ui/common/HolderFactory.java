package com.notesforcontacts.sample.ui.common;

import android.view.ViewGroup;

public abstract class HolderFactory<M>
{
    public abstract BaseViewHolder<M> buildHolder(ViewGroup viewGroup);
}