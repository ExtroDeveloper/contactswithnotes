package com.notesforcontacts.sample.ui.notelist.note;

import java.io.Serializable;

public class NoteForContact implements Serializable
{
    public NoteForContact(long id, String text, boolean checked)
    {
        this.id = id;
        this.checked = checked;
        this.text = text;
    }

    public final long id;
    public final boolean checked;
    public final String text;
}