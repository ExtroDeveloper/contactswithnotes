package com.notesforcontacts.sample.ui.common;

public abstract class Feature
{
    public void stop()
    {
    }

    public void resume()
    {
    }
}
