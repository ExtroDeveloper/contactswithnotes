package com.notesforcontacts.sample.ui.contactpage.filter;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import com.notesforcontacts.sample.BR;

public class ViewModel extends BaseObservable
{
    public ViewModel(@NonNull String nameOrPhone, boolean onlyWithNotes)
    {
        this.nameOrPhone = nameOrPhone;
        this.onlyWithNotes = onlyWithNotes;
    }

    public void onOkClick()
    {
        actionOnOkClick.run();
    }

    @Bindable
    public boolean isOnlyWithNotes()
    {
        return onlyWithNotes;
    }

    public void setOnlyWithNotes(boolean onlyWithNotes)
    {
        this.onlyWithNotes = onlyWithNotes;
        notifyPropertyChanged(BR.onlyWithNotes);
    }

    @Bindable
    public String getNameOrPhone()
    {
        return nameOrPhone;
    }

    public void setNameOrPhone(@NonNull String nameOrPhone)
    {
        this.nameOrPhone = nameOrPhone;
        notifyPropertyChanged(BR.nameOrPhone);
    }

    public void setActionOnOkClick(Runnable actionOnOkClick)
    {
        this.actionOnOkClick = actionOnOkClick;
    }

    private String nameOrPhone;
    private boolean onlyWithNotes;
    private Runnable actionOnOkClick;
}
