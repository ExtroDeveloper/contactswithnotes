package com.notesforcontacts.sample.ui.common;

import android.databinding.BindingAdapter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.notesforcontacts.sample.ui.contactpage.contacts.ContactViewHolder;
import com.notesforcontacts.sample.ui.contactpage.contacts.ContactViewModel;
import com.notesforcontacts.sample.ui.contactpage.contacts.ContactItemDiffCallback;
import com.notesforcontacts.sample.ui.notelist.note.NoteItemDiffCallback;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewHolder;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModel;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

public class BindingAdapters
{
    @BindingAdapter("onLongClick")
    public static void onLongClick(View view, Runnable onLongClick)
    {
        view.setOnLongClickListener(view1 ->
        {
            onLongClick.run();
            return true;
        });
    }

    @BindingAdapter({"imageUrl", "placeholder"})
    public static void loadImageWithPlaceholder(ImageView view, Uri url, Drawable placeholder)
    {
        Picasso.with(
                view.getContext())
                .load(url)
                .error(placeholder)
                .placeholder(placeholder)
                .into(view);
    }

    @BindingAdapter("items")
    public static void setAdapterAndNoteViewModelItems(EasyRecyclerView recyclerView, List<NoteViewModel> items)
    {
        @SuppressWarnings("unchecked")
        FactoryAdapter<NoteViewModel> adapter = (FactoryAdapter<NoteViewModel>) recyclerView.getAdapter();
        if (adapter == null)
        {
            adapter = new FactoryAdapter<>(recyclerView.getContext(), new NoteViewHolder.Factory());
            adapter.set(Collections.emptyList());
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        }

        if (items != null)
        {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new NoteItemDiffCallback(adapter.getItems(), items));
            adapter.setWithNoUpdate(items);
            result.dispatchUpdatesTo(adapter);
        }

        if (adapter.isEmpty())
        {
            recyclerView.showEmpty();
        }
    }

    @BindingAdapter("items")
    public static void setAdapterAndContactViewModelItems(EasyRecyclerView recyclerView, List<ContactViewModel> items)
    {
        @SuppressWarnings("unchecked")
        FactoryAdapter<ContactViewModel> adapter = (FactoryAdapter<ContactViewModel>) recyclerView.getAdapter();
        if (adapter == null)
        {
            adapter = new FactoryAdapter<>(recyclerView.getContext(), new ContactViewHolder.Factory());
            adapter.set(Collections.emptyList());
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        }

        if (items != null)
        {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new ContactItemDiffCallback(adapter.getItems(), items));
            adapter.setWithNoUpdate(items);
            result.dispatchUpdatesTo(adapter);
        }

        if (adapter.isEmpty())
        {
            recyclerView.showEmpty();
        }
    }

    @BindingAdapter("android:onClick")
    public static void onClick(View view, Runnable runnable)
    {
        view.setOnClickListener(view1 -> runnable.run());
    }

    @BindingAdapter("strike")
    public static void setStrike(TextView text, boolean strike)
    {
        if (strike)
        {
            text.setPaintFlags(text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else
        {
            text.setPaintFlags(text.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }
}