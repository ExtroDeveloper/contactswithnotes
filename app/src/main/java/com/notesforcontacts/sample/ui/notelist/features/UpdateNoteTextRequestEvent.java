package com.notesforcontacts.sample.ui.notelist.features;

public class UpdateNoteTextRequestEvent
{
    public UpdateNoteTextRequestEvent(long id, String text)
    {
        this.id = id;
        this.text = text;
    }

    public final long id;
    public final String text;
}
