package com.notesforcontacts.sample.ui.notelist.model;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.Storages.ContactInfoFetcher;
import com.notesforcontacts.sample.Storages.NoteStorage;
import com.notesforcontacts.sample.ui.common.NoteStorageUpdateObserver;

import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Model
{
    @NonNull
    public static Model getNewInstanceForAllNotes(String contactId, ContactInfoFetcher contactInfoFetcher, NoteStorage noteStorage)
    {
        Model model = new Model();
        model.contactId = contactId;
        model.contactInfoFetcher = contactInfoFetcher;
        model.noteStorage = noteStorage;
        model.observableNoteList = Observable.defer(() -> Observable.create(new NoteStorageUpdateObserver()).map(Integer -> model.getNoteList()));

        return model;
    }

    public String getNoteText(long noteId)
    {
        return getNote(noteId).text;
    }

    public void deleteNote(long id)
    {
        noteStorage.deleteNote(id);
    }

    public void addNote(String text)
    {
        noteStorage.addNote(contactId, text);
    }

    public void updateNoteText(long noteId, String text)
    {
        noteStorage.updateNoteText(noteId, text);
    }

    public String getContactName()
    {
        return Observable.from(contactInfoFetcher.getContactInfoList())
                .first(contactInfo -> contactInfo.id.equals(contactId))
                .subscribeOn(Schedulers.newThread())
                .toBlocking().first().name;
    }

    public Subscription subscribeToNoteList(Action1<List<NoteForContact>> action)
    {
        return observableNoteList.subscribe(action);
    }

    public void invertCheckedState(long id)
    {
        NoteForContact note = getNote(id);

        if (note.checked)
        {
            noteStorage.updateNoteCheckedState(id, false);
        }
        else
        {
            noteStorage.updateNoteCheckedState(id, true);
        }
    }

    private Observable<List<NoteForContact>> observableNoteList;
    private ContactInfoFetcher contactInfoFetcher;
    private String contactId;
    private NoteStorage noteStorage;

    private List<NoteForContact> getNoteList()
    {
        return noteStorage.getAllNotesForContactId(contactId);
    }

    private NoteForContact getNote(long id)
    {
        return Observable.just(getNoteList())
                .flatMap(new Func1<List<NoteForContact>, Observable<NoteForContact>>()
                {
                    @Override
                    public Observable<NoteForContact> call(List<NoteForContact> noteForContacts)
                    {
                        return Observable.from(noteForContacts);
                    }
                })
                .first(note -> note.id == id)
                .subscribeOn(Schedulers.newThread())
                .toBlocking().first();
    }
}