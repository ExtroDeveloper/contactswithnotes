package com.notesforcontacts.sample.ui.contactpage.model;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.Storages.ContactInfoFetcher;
import com.notesforcontacts.sample.Storages.FilterStorage;
import com.notesforcontacts.sample.Storages.NoteStorage;
import com.notesforcontacts.sample.contacts.ContactInfo;
import com.notesforcontacts.sample.contacts.FilterByNameHasNote;
import com.notesforcontacts.sample.ui.common.NoteStorageUpdateObserver;

import java.util.LinkedList;
import java.util.List;

import rx.Observable;

public class Model
{
    public Model(@NonNull ContactInfoFetcher contactInfoFetcher, @NonNull FilterStorage filterStorage, @NonNull NoteStorage noteStorage)
    {
        this.noteStorage = noteStorage;
        this.contactInfoFetcher = contactInfoFetcher;
        this.observableFilter = filterStorage.getObservable();
        this.observableContactInfoList = getContactInfoFetcher();
    }

    public Observable<List<ContactInfo>> getObservableContactInfoList()
    {
        return observableContactInfoList;
    }

    private final Observable<FilterByNameHasNote> observableFilter;
    private final Observable<List<ContactInfo>> observableContactInfoList;
    private final ContactInfoFetcher contactInfoFetcher;
    private final NoteStorage noteStorage;

    private Observable<List<ContactInfo>> getContactInfoFetcher()
    {
        return Observable.combineLatest(
                Observable.combineLatest(
                        Observable.just(contactInfoFetcher.getContactInfoList()),
                        Observable.create(new NoteStorageUpdateObserver()).map(Integer -> noteStorage.getNotesByContactIds()),
                        (contacts, notesByContactIds) ->
                        {
                            for (ContactInfo contact : contacts)
                            {
                                List<NoteForContact> notes = notesByContactIds.get(contact.id);
                                contact.noteCount = notes != null ? notes.size() : 0;
                            }

                            return contacts;
                        }),
                observableFilter,
                (contacts, filter) ->
                {
                    List<ContactInfo> list = new LinkedList<>();

                    for (ContactInfo contactInfo : contacts)
                    {
                        if (filter.suitable(contactInfo))
                        {
                            list.add(contactInfo);
                        }
                    }

                    return list;
                });
    }
}