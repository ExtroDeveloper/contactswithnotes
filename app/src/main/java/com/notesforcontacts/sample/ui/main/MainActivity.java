package com.notesforcontacts.sample.ui.main;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.notesforcontacts.sample.R;
import com.notesforcontacts.sample.Storages.MigrationFromOldDb;
import com.notesforcontacts.sample.Storages.SettingsStorage;
import com.notesforcontacts.sample.Storages.Storage;
import com.notesforcontacts.sample.Utilits.EventManager;
import com.notesforcontacts.sample.ui.contactpage.ContactClickEvent;
import com.notesforcontacts.sample.ui.contactpage.ContactListFragment;
import com.notesforcontacts.sample.ui.notelist.NoteListInAppFragmentBuilder;
import com.squareup.otto.Subscribe;

import hotchemi.android.rate.AppRate;

import static android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences;

public class MainActivity extends AppCompatActivity
{
    public static String contactIdKey = "contactId";

    @Override
    protected void onPause()
    {
        EventManager.unregister(eventListener);
        super.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        showContactDetailsIfPassed();
        EventManager.register(eventListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setLocale();
        setTitle();
        initView();
        initEventListener();
        migrateDbIfNeed();
        appRateRequestIfNeeded();
        showContactListIfFirstStart(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        setLocale();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (!Settings.canDrawOverlays(this))
            {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));

                int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 5469;
                startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
            }
        }
    }

    private Object eventListener;

    private void showContactListIfFirstStart(Bundle savedInstanceState)
    {
        if (savedInstanceState == null)
        {
            showContactList();
        }
    }

    private void initView()
    {
        setContentView(R.layout.activity_main);
    }

    private void setTitle()
    {
        setTitle(R.string.app_name);
    }

    private void setLocale()
    {
        String local = getDefaultSharedPreferences(this).getString("lang", "en");
        SettingsStorage.setLocale(this, local);
    }

    private void showContactDetailsIfPassed()
    {
        String contactId = getIntent().getStringExtra(contactIdKey);

        if (contactId != null)
        {
            showDetails(contactId);
        }
    }

    private void migrateDbIfNeed()
    {
        if (needDbUpdate())
        {
            migrateDbToORM();
            setNoNeedDbUpdate();
        }
    }

    private void migrateDbToORM()
    {
        MigrationFromOldDb.migrateToORM(this);
    }

    private void initEventListener()
    {
        eventListener = new Object()
        {
            @Subscribe
            public void handleEvent(ContactClickEvent event)
            {
                String contactId = event.contactId;

                showDetails(contactId);
            }
        };
    }

    private void showContactList()
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container, new ContactListFragment());
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void setNoNeedDbUpdate()
    {
        Storage.saveBoolean(this, "need_db_update", false);
    }

    @NonNull
    private Boolean needDbUpdate()
    {
        return Storage.loadBoolean(this, "need_db_update", true);
    }

    private void appRateRequestIfNeeded()
    {
        AppRate.with(this).monitor();
        AppRate.showRateDialogIfMeetsConditions(this);
    }

    private void showDetails(String contactId)
    {
        Fragment notesListInApp = new NoteListInAppFragmentBuilder(contactId).build();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right, R.anim.slide_in_left, R.anim.slide_in_right);
        transaction.add(R.id.container, notesListInApp);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }
}