package com.notesforcontacts.sample.ui.notelist.note;

import com.notesforcontacts.sample.ui.notelist.model.Model;

public class NoteDeleter
{
    public NoteDeleter(Model model)
    {
        this.model = model;
    }

    public void deleteNote(long id)
    {
        model.deleteNote(id);
    }

    private final Model model;
}
