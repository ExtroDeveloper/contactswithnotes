package com.notesforcontacts.sample.ui.common;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import java.util.List;

public abstract class BaseDiffCallback<T> extends DiffUtil.Callback
{
    public BaseDiffCallback(@NonNull List<T> oldList, @NonNull List<T> newList)
    {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize()
    {
        return oldList.size();
    }

    @Override
    public int getNewListSize()
    {
        return newList.size();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition)
    {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }

    protected final List<T> oldList;
    protected final List<T> newList;
}