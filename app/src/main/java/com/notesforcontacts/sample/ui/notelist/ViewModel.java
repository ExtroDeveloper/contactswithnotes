package com.notesforcontacts.sample.ui.notelist;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import com.notesforcontacts.sample.BR;
import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModel;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModelFactory;

import java.util.ArrayList;
import java.util.List;

public class ViewModel extends BaseObservable
{
    public ViewModel(@NonNull NoteViewModelFactory noteViewModelFactory, @com.android.annotations.NonNull NoteAdder noteAdder)
    {
        this.noteViewModelFactory = noteViewModelFactory;
        this.noteAdder = noteAdder;
    }

    @Bindable
    public String getUserName()
    {
        return userName;
    }

    public void setUserName(@NonNull String userName)
    {
        this.userName = userName;
        notifyPropertyChanged(BR.userName);
    }

    public void onAddNoteClick()
    {
        noteAdder.addNewNote();
    }

    @Bindable
    public List<NoteViewModel> getNoteViewModels()
    {
        return noteViewModels;
    }

    public void setNotes(@NonNull List<NoteForContact> notes)
    {
        noteViewModels.clear();
        for (int i = 0; i < notes.size(); ++i)
        {
            NoteForContact note = notes.get(i);

            NoteViewModel noteViewModel = noteViewModelFactory.createViewModel(note);

            noteViewModels.add(noteViewModel);
        }

        notifyPropertyChanged(BR.noteViewModels);
    }

    private final List<NoteViewModel> noteViewModels = new ArrayList<>();
    private final NoteViewModelFactory noteViewModelFactory;
    private final NoteAdder noteAdder;
    private String userName;
}