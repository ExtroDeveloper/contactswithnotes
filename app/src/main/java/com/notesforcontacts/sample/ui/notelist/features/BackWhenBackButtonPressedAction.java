package com.notesforcontacts.sample.ui.notelist.features;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

public class BackWhenBackButtonPressedAction
{
    public BackWhenBackButtonPressedAction(@NonNull Fragment activityBackPresser)
    {
        this.fragment = activityBackPresser;
    }

    public void run()
    {
        fragment.getActivity().onBackPressed();
    }

    private final Fragment fragment;
}