package com.notesforcontacts.sample.ui.contactpage;

import com.notesforcontacts.sample.contacts.ContactInfo;
import com.notesforcontacts.sample.ui.contactpage.contacts.ContactViewModel;
import com.notesforcontacts.sample.ui.contactpage.contacts.ContactViewModelCallback;

public class ContactViewModelFactory
{
    public ContactViewModelFactory(NoteListPageOpenRequester noteListPageOpenRequester, ContactActionShower contactActionShower)
    {
        this.noteListPageOpenRequester = noteListPageOpenRequester;
        this.contactActionShower = contactActionShower;
    }

    public ContactViewModel createViewModel(ContactInfo contactInfo)
    {
        ContactViewModel.Callback callback = new ContactViewModelCallback(contactInfo.id, noteListPageOpenRequester, contactActionShower);

        return new ContactViewModel(contactInfo.name, contactInfo.phoneNumberToString(), String.valueOf(contactInfo.noteCount), contactInfo.imageUri, callback);
    }

    private final NoteListPageOpenRequester noteListPageOpenRequester;
    private final ContactActionShower contactActionShower;
}
