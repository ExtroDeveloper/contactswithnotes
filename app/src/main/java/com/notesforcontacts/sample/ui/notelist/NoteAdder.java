package com.notesforcontacts.sample.ui.notelist;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.ui.notelist.features.EditNoteDialogShower;

public class NoteAdder
{
    public NoteAdder(@NonNull EditNoteDialogShower editNoteDialogShower)
    {
        this.editNoteDialogShower = editNoteDialogShower;
    }

    public void addNewNote()
    {
        editNoteDialogShower.showAddNoteEditDialog();
    }

    private final EditNoteDialogShower editNoteDialogShower;
}
