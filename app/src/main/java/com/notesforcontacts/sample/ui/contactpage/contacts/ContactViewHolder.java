package com.notesforcontacts.sample.ui.contactpage.contacts;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.notesforcontacts.sample.databinding.ContactListItemBinding;
import com.notesforcontacts.sample.ui.common.BaseViewHolder;
import com.notesforcontacts.sample.ui.common.HolderFactory;

public class ContactViewHolder extends BaseViewHolder<ContactViewModel>
{
    public static class Factory extends HolderFactory<ContactViewModel>
    {
        @Override
        public BaseViewHolder<ContactViewModel> buildHolder(ViewGroup parent)
        {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ContactListItemBinding binding = ContactListItemBinding.inflate(inflater, parent, true);

            return new ContactViewHolder(binding);
        }
    }

    @Override
    public void setData(ContactViewModel data)
    {
        binding.setViewModel(data);
    }
    private final ContactListItemBinding binding;

    private ContactViewHolder(ContactListItemBinding binding)
    {
        super(binding.getRoot());

        this.binding = binding;
    }
}