package com.notesforcontacts.sample.ui.notelist.features;

public interface EditNoteDialogShower
{
    void showNoteEditDialog(long noteId, String text);

    void showAddNoteEditDialog();
}
