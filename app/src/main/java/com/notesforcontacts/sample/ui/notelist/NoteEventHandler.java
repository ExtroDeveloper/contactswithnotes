package com.notesforcontacts.sample.ui.notelist;

import com.android.annotations.NonNull;
import com.notesforcontacts.sample.Utilits.EventManager;
import com.notesforcontacts.sample.ui.common.Feature;
import com.notesforcontacts.sample.ui.notelist.features.AddNoteRequestEvent;
import com.notesforcontacts.sample.ui.notelist.features.UpdateNoteTextRequestEvent;
import com.notesforcontacts.sample.ui.notelist.model.Model;
import com.squareup.otto.Subscribe;

public class NoteEventHandler extends Feature
{
    public NoteEventHandler(@NonNull Model model)
    {
        this.model = model;
    }

    @Subscribe
    public void handle(@NonNull AddNoteRequestEvent event)
    {
        model.addNote(event.text);
    }

    @Subscribe
    public void handle(UpdateNoteTextRequestEvent event)
    {
        model.updateNoteText(event.id, event.text);
    }

    @Override
    public void stop()
    {
        EventManager.unregister(this);
    }

    @Override
    public void resume()
    {
        EventManager.register(this);
    }

    private final Model model;
}
