package com.notesforcontacts.sample.ui.contactpage;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.notesforcontacts.sample.BR;
import com.notesforcontacts.sample.contacts.ContactInfo;
import com.notesforcontacts.sample.ui.contactpage.contacts.ContactViewModel;

import java.util.ArrayList;
import java.util.List;

public class ViewModel extends BaseObservable
{
    public ViewModel(ContactViewModelFactory contactViewModelFactory)
    {
        this.contactViewModelFactory = contactViewModelFactory;
    }

    @Bindable
    public List<ContactViewModel> getContactViewModels()
    {
        return contactViewModels;
    }

    public void setContactInfos(List<ContactInfo> contactInfos)
    {
        notifyPropertyChanged(BR.contactViewModels);

        contactViewModels.clear();
        for (ContactInfo contactInfo : contactInfos)
        {
            ContactViewModel noteViewModel = contactViewModelFactory.createViewModel(contactInfo);

            contactViewModels.add(noteViewModel);
        }

        notifyPropertyChanged(BR.noteViewModels);
    }

    @Bindable
    public boolean isHighlightFilterIcon()
    {
        return highlightFilterIcon;
    }

    public void setHighlightFilterIcon(boolean highlightFilterIcon)
    {
        this.highlightFilterIcon = highlightFilterIcon;
        notifyPropertyChanged(BR.highlightFilterIcon);
    }

    private final ContactViewModelFactory contactViewModelFactory;
    private final List<ContactViewModel> contactViewModels = new ArrayList<>();
    private boolean highlightFilterIcon;
}