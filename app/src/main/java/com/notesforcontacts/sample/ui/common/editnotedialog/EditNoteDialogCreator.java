package com.notesforcontacts.sample.ui.common.editnotedialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.notesforcontacts.sample.R;
import com.notesforcontacts.sample.Utilits.EventManager;
import com.notesforcontacts.sample.ui.notelist.features.AddNoteRequestEvent;
import com.notesforcontacts.sample.ui.notelist.features.UpdateNoteTextRequestEvent;

public class EditNoteDialogCreator
{
    public EditNoteDialogCreator(@NonNull Context context, @NonNull Params params)
    {
        this.context = context;
        this.params = params;
    }

    public AlertDialog create()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final EditText editText = (EditText) View.inflate(context, R.layout.six_line_edit_text, null);
        editText.setText(params.sourceText);

        builder.setView(editText);
        builder.setPositiveButton(android.R.string.ok, (dialog, whichButton) -> requestToSaveNote(editText.getText().toString()));

        final AlertDialog dialog = builder.create();

        editText.requestFocus();

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
        return dialog;
    }

    private final Params params;
    private final Context context;

    private void requestToSaveNote(@NonNull String text)
    {
        if (params.noteId == Params.noNoteId)
        {
            EventManager.getInstance().post(new AddNoteRequestEvent(text));
            return;
        }

        if (params.sourceText.equals(text))
        {
            return;
        }

        EventManager.getInstance().post(new UpdateNoteTextRequestEvent(params.noteId, text));
    }
}
