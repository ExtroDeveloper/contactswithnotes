package com.notesforcontacts.sample.ui.notelist.note;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.notesforcontacts.sample.databinding.NotesListItemBinding;
import com.notesforcontacts.sample.ui.common.BaseViewHolder;
import com.notesforcontacts.sample.ui.common.HolderFactory;

public class NoteViewHolder extends BaseViewHolder<NoteViewModel>
{
    public static class Factory extends HolderFactory<NoteViewModel>
    {
        @Override
        public BaseViewHolder<NoteViewModel> buildHolder(ViewGroup parent)
        {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            NotesListItemBinding binding = NotesListItemBinding.inflate(inflater, parent, true);

            return new NoteViewHolder(binding);
        }
    }

    @Override
    public void setData(NoteViewModel data)
    {
        binding.setViewModel(data);
    }
    private final NotesListItemBinding binding;

    private NoteViewHolder(NotesListItemBinding binding)
    {
        super(binding.getRoot());

        this.binding = binding;
    }
}