package com.notesforcontacts.sample.ui.notelist.features;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.ui.notelist.note.NoteForContact;
import com.notesforcontacts.sample.ui.common.Feature;
import com.notesforcontacts.sample.ui.notelist.ViewModel;
import com.notesforcontacts.sample.ui.notelist.model.Model;

import java.util.List;

import rx.Subscription;

public class NoteListShower extends Feature
{
    public NoteListShower(@NonNull ViewModel viewModel, @NonNull Model model)
    {
        this.model = model;
        this.viewModel = viewModel;
    }

    @Override
    public void stop()
    {
        subscription.unsubscribe();
    }

    @Override
    public void resume()
    {
        subscription = model.subscribeToNoteList(this::showNotes);
    }

    private final ViewModel viewModel;
    private final Model model;
    private Subscription subscription;

    private void showNotes(List<NoteForContact> noteForContacts)
    {
        viewModel.setNotes(noteForContacts);
    }
}