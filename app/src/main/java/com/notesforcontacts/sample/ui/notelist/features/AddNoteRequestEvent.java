package com.notesforcontacts.sample.ui.notelist.features;

public class AddNoteRequestEvent
{
    public AddNoteRequestEvent(String text)
    {
        this.text = text;
    }

    public final String text;
}
