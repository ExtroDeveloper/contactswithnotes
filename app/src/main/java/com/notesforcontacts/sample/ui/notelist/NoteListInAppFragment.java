package com.notesforcontacts.sample.ui.notelist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.notesforcontacts.sample.Storages.ContactInfoFetcher;
import com.notesforcontacts.sample.Storages.NoteStorage;
import com.notesforcontacts.sample.databinding.NoteListPageBinding;
import com.notesforcontacts.sample.ui.common.Feature;
import com.notesforcontacts.sample.ui.notelist.features.BackWhenBackButtonPressedAction;
import com.notesforcontacts.sample.ui.notelist.features.ContactNameShower;
import com.notesforcontacts.sample.ui.notelist.features.EditNoteDialogShower;
import com.notesforcontacts.sample.ui.notelist.features.FragmentEditNoteDialogShower;
import com.notesforcontacts.sample.ui.notelist.features.NoteListShower;
import com.notesforcontacts.sample.ui.notelist.features.ShareNoteAction;
import com.notesforcontacts.sample.ui.notelist.model.Model;
import com.notesforcontacts.sample.ui.notelist.note.LongClickAction;
import com.notesforcontacts.sample.ui.notelist.note.NoteCheckedStateInverter;
import com.notesforcontacts.sample.ui.notelist.note.NoteDeleter;
import com.notesforcontacts.sample.ui.notelist.note.NoteViewModelFactory;
import com.orm.util.Collection;

import java.util.List;

@FragmentWithArgs
public class NoteListInAppFragment extends Fragment
{
    @Arg
    public String contactId;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FragmentArgs.inject(this);
        setRetainInstance(true);
        initModel();
        initViewModel();
        initFeatureList();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        NoteListPageBinding binding = NoteListPageBinding.inflate(inflater, container, false);
        binding.setViewModel(viewModel);
        binding.setBackPressedHandler(new BackWhenBackButtonPressedAction(this));
        binding.executePendingBindings();

        return binding.getRoot();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        for (Feature feature : featureList)
        {
            feature.resume();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        for (Feature feature : featureList)
        {
            feature.stop();
        }
    }

    private List<Feature> featureList;
    private ViewModel viewModel;
    private Model model;

    private void initFeatureList()
    {
        featureList = Collection.list(
                new NoteListShower(viewModel, model),
                new ContactNameShower(viewModel, model),
                new NoteEventHandler(model));
    }

    private void initModel()
    {
        model = Model.getNewInstanceForAllNotes(contactId, new ContactInfoFetcher(), new NoteStorage());
    }

    private void initViewModel()
    {
        EditNoteDialogShower editNoteDialogShower = new FragmentEditNoteDialogShower(this);
        NoteDeleter noteDeleter = new NoteDeleter(model);
        NoteCheckedStateInverter noteCheckedStateInverter = new NoteCheckedStateInverter(model);
        LongClickAction longClickAction = new ShareNoteAction(getContext());
        NoteViewModelFactory noteViewModelFactory = new NoteViewModelFactory(editNoteDialogShower, noteDeleter, noteCheckedStateInverter, longClickAction);

        NoteAdder noteAdder = new NoteAdder(editNoteDialogShower);

        viewModel = new ViewModel(noteViewModelFactory, noteAdder);
    }
}