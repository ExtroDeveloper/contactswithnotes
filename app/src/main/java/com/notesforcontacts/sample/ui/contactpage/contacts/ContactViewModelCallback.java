package com.notesforcontacts.sample.ui.contactpage.contacts;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.ui.contactpage.ContactActionShower;
import com.notesforcontacts.sample.ui.contactpage.NoteListPageOpenRequester;

public class ContactViewModelCallback implements ContactViewModel.Callback
{
    public ContactViewModelCallback(@NonNull String contactId, @NonNull NoteListPageOpenRequester noteListPageOpenRequester, @NonNull ContactActionShower contactActionShower)
    {
        this.contactId = contactId;
        this.noteListPageOpenRequester = noteListPageOpenRequester;
        this.contactActionShower = contactActionShower;
    }

    @Override
    public void onClick()
    {
        noteListPageOpenRequester.requestToOpenNoteListForContact(contactId);
    }

    @Override
    public void onLongClick()
    {
        contactActionShower.showAction(contactId);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }

        if (getClass() != obj.getClass())
        {
            return false;
        }

        ContactViewModelCallback toCompare = (ContactViewModelCallback) obj;

        return contactId.equals(toCompare.contactId)
                && noteListPageOpenRequester.equals(toCompare.noteListPageOpenRequester)
                && contactActionShower.equals(toCompare.contactActionShower);
    }

    private final String contactId;
    private final NoteListPageOpenRequester noteListPageOpenRequester;
    private final ContactActionShower contactActionShower;
}