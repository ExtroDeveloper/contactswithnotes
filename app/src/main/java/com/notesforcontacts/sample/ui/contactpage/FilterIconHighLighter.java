package com.notesforcontacts.sample.ui.contactpage;

import com.notesforcontacts.sample.Storages.FilterStorage;
import com.notesforcontacts.sample.contacts.FilterByNameHasNote;
import com.notesforcontacts.sample.ui.common.Feature;

import rx.Observable;
import rx.Subscription;

public class FilterIconHighLighter extends Feature
{
    public FilterIconHighLighter(ViewModel viewModel, FilterStorage filterStorage, Observable<Void> menuReady)
    {
        this.viewModel = viewModel;
        observable = Observable.combineLatest(filterStorage.getObservable(), menuReady, (filterByNameHasNote, b) -> filterByNameHasNote);
    }

    @Override
    public void stop()
    {
        subscription.unsubscribe();
    }

    @Override
    public void resume()
    {
        subscription = observable.subscribe(filterByNameHasNote ->
        {
            viewModel.setHighlightFilterIcon(!filterByNameHasNote.getName().isEmpty() || filterByNameHasNote.isOnlyWithNotes());
        });
    }

    private final Observable<FilterByNameHasNote> observable;
    private final ViewModel viewModel;
    private Subscription subscription;
}