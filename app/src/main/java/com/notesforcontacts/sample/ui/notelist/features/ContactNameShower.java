package com.notesforcontacts.sample.ui.notelist.features;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.ui.common.Feature;
import com.notesforcontacts.sample.ui.notelist.ViewModel;
import com.notesforcontacts.sample.ui.notelist.model.Model;

public class ContactNameShower extends Feature
{
    public ContactNameShower(@NonNull ViewModel viewModel, @NonNull Model model)
    {
        this.viewModel = viewModel;
        this.model = model;
    }

    @Override
    public void resume()
    {
        viewModel.setUserName(model.getContactName());
    }

    private final ViewModel viewModel;
    private final Model model;
}