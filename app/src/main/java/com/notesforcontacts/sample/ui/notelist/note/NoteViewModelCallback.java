package com.notesforcontacts.sample.ui.notelist.note;

import android.support.annotation.NonNull;

import com.notesforcontacts.sample.ui.notelist.features.EditNoteDialogShower;

public class NoteViewModelCallback implements NoteViewModel.Callback
{
    public NoteViewModelCallback(long noteId, @NonNull NoteCheckedStateInverter noteCheckedStateInverter, @NonNull EditNoteDialogShower editNoteDialogShower, @NonNull NoteDeleter noteDeleter, @NonNull LongClickAction longClickAction)
    {
        this.noteId = noteId;
        this.noteCheckedStateInverter = noteCheckedStateInverter;
        this.editNoteDialogShower = editNoteDialogShower;
        this.noteDeleter = noteDeleter;
        this.longClickAction = longClickAction;
    }

    @Override
    public void onTextPressed()
    {
        noteCheckedStateInverter.invertCheckedState(noteId);
    }

    @Override
    public void onDeleteNoteButton()
    {
        noteDeleter.deleteNote(noteId);
    }

    @Override
    public void onEditNoteButton(String text)
    {
        editNoteDialogShower.showNoteEditDialog(noteId, text);
    }

    @Override
    public void onLongClick(String text)
    {
        longClickAction.run(text);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }

        if (getClass() != obj.getClass())
        {
            return false;
        }

        NoteViewModelCallback toCompare = (NoteViewModelCallback) obj;

        return noteId == toCompare.noteId
                && noteCheckedStateInverter.equals(toCompare.noteCheckedStateInverter)
                && editNoteDialogShower.equals(toCompare.editNoteDialogShower)
                && noteDeleter.equals(toCompare.noteDeleter)
                && longClickAction.equals(toCompare.longClickAction);
    }

    private final long noteId;
    private final NoteCheckedStateInverter noteCheckedStateInverter;
    private final EditNoteDialogShower editNoteDialogShower;
    private final NoteDeleter noteDeleter;
    private final LongClickAction longClickAction;
}