package com.notesforcontacts.sample.ui.contactpage.contacts;

import com.notesforcontacts.sample.ui.common.BaseDiffCallback;
import com.notesforcontacts.sample.ui.contactpage.contacts.ContactViewModel;

import java.util.List;

public class ContactItemDiffCallback extends BaseDiffCallback<ContactViewModel>
{
    public ContactItemDiffCallback(List<ContactViewModel> oldList, List<ContactViewModel> newList)
    {
        super(oldList, newList);
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition)
    {
        return oldList.get(oldItemPosition).getName().equals(newList.get(newItemPosition).getName());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition)
    {
        return oldList.equals(newList.get(newItemPosition));
    }
}
