package com.notesforcontacts.sample.ui.contactpage;

import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

public class ContactActionShower
{
    public ContactActionShower(@NonNull Fragment fragment)
    {
        this.fragment = fragment;
    }

    public void showAction(@NonNull String contactId)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactId));
        intent.setData(uri);
        fragment.startActivity(intent);
    }

    private final Fragment fragment;
}