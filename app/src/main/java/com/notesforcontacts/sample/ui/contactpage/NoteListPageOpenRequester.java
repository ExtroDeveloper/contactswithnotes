package com.notesforcontacts.sample.ui.contactpage;

import com.notesforcontacts.sample.Utilits.EventManager;

public class NoteListPageOpenRequester
{
    public void requestToOpenNoteListForContact(String contactId)
    {
        EventManager.postEvent(new ContactClickEvent(contactId));
    }
}
