package com.notesforcontacts.sample.ui.common;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

@SuppressWarnings("unused")
abstract public class BaseViewHolder<M> extends RecyclerView.ViewHolder
{
    public BaseViewHolder(View itemView)
    {
        super(itemView);
    }

    public BaseViewHolder(ViewGroup parent, @LayoutRes int res)
    {
        super(LayoutInflater.from(parent.getContext()).inflate(res, parent, false));
    }

    public void setData(M data)
    {
    }

    protected <T extends View> T $(@IdRes int id)
    {
        //noinspection unchecked
        return (T) itemView.findViewById(id);
    }

    protected Context getContext()
    {
        return itemView.getContext();
    }
}