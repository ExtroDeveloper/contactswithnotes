package com.notesforcontacts.sample;

import android.app.Application;

import com.orm.SugarContext;

public class NotesForContactsApplication extends Application
{
    public static NotesForContactsApplication application;

    @Override
    public void onCreate()
    {
        super.onCreate();
        SugarContext.init(this);
        application = this;
    }

    @Override
    public void onTerminate()
    {
        super.onTerminate();
        SugarContext.terminate();
    }
}