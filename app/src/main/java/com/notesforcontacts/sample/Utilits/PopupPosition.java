package com.notesforcontacts.sample.Utilits;

public enum PopupPosition
{
    TOP,
    BOTTOM,
    DONT_SHOW
}