package com.notesforcontacts.sample.Utilits;

import com.android.annotations.NonNull;
import com.squareup.otto.Bus;

public class EventManager
{
    public static void postEvent(@NonNull Object event)
    {
        getInstance().post(event);
    }

    public static void register(@NonNull Object event)
    {
        getInstance().bus.register(event);
    }

    public static void unregister(@NonNull Object event)
    {
        getInstance().bus.unregister(event);
    }

    public static EventManager getInstance()
    {
        if (instance == null)
        {
            instance = new EventManager();
        }
        return instance;
    }

    public void post(@NonNull Object event)
    {
        bus.post(event);
    }

    private static EventManager instance = null;
    private final Bus bus;

    private EventManager()
    {
        bus = new Bus();
    }
}