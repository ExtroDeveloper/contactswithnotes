package com.notesforcontacts.sample.contacts;

import android.support.annotation.NonNull;

interface Filter
{
    boolean suitable(@NonNull ContactInfo contactInfo);
}