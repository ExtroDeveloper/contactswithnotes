package com.notesforcontacts.sample.contacts;

import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ContactInfo
{
    public ContactInfo(@NonNull String id, @NonNull List<String> phoneNumbers, @NonNull String name, int noteCount, @NonNull Uri imageUri)
    {
        this.id = id;
        this.phoneNumbers = phoneNumbers;
        this.name = name;
        this.noteCount = noteCount;
        this.imageUri = imageUri;
    }

    public final String id;
    public final List<String> phoneNumbers;
    public final String name;
    public final Uri imageUri;
    public int noteCount;

    public String phoneNumberToString()
    {
        final StringBuilder result = new StringBuilder();

        Set<String> numbersToShow = new HashSet<>();

        for (String number : phoneNumbers)
        {
            String onlyDigitsFromPhoneNumber = removeNotDigits(number);

            if (numbersToShow.contains(onlyDigitsFromPhoneNumber))
            {
                continue;
            }

            numbersToShow.add(onlyDigitsFromPhoneNumber);

            if (result.length() > 0)
            {
                result.append("\n");
            }

            result.append(number);
        }

        return result.toString();
    }

    private String removeNotDigits(@NonNull String number)
    {
        return number.replaceAll("\\D", "");
    }
}