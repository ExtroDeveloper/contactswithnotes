package com.notesforcontacts.sample.contacts;

import android.support.annotation.NonNull;

public class FilterByNameHasNote implements Filter
{
    public FilterByNameHasNote(@NonNull String lowerCasedName, boolean onlyWithNotes)
    {
        this.lowerCasedName = lowerCasedName.toLowerCase();
        this.onlyWithNotes = onlyWithNotes;
    }

    public boolean isOnlyWithNotes()
    {
        return onlyWithNotes;
    }

    public String getName()
    {
        return lowerCasedName;
    }

    @Override
    public boolean suitable(@NonNull ContactInfo contactInfo)
    {
        return (nameContains(contactInfo)
                || phoneContains(contactInfo))
                && (!onlyWithNotes || contactInfo.noteCount > 0);
    }

    private final String lowerCasedName;
    private final boolean onlyWithNotes;

    private boolean phoneContains(@NonNull ContactInfo contactInfo)
    {
        for (String number : contactInfo.phoneNumbers)
        {
            if (number.contains(lowerCasedName) || number.replaceAll("\\D", "").contains(lowerCasedName))
            {
                return true;
            }
        }
        return false;
    }

    private boolean nameContains(@NonNull ContactInfo contactInfo)
    {
        return contactInfo.name.toLowerCase().contains(lowerCasedName);
    }
}